//
//  APPSConcurrentOperation.m
//  TRAVERSE
//
//  Created by Petr Yanenko on 9/4/15.
//  Copyright (c) 2015 Applikey Solutions. All rights reserved.
//

#import "APPSConcurrentOperation.h"
#import "APPSGlobalDataContext.h"

@implementation APPSConcurrentOperation

@synthesize executing = _executing, finished = _finished;

- (RACScheduler *)scheduler {
    static  RACScheduler *scheduler;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        scheduler = [RACScheduler schedulerWithPriority:RACSchedulerPriorityHigh];
    });
    return scheduler;
}

- (BOOL)isExecuting {
    return _executing;
}

- (BOOL)finished {
    return _finished;
}

- (void)setExecuting:(BOOL)executing {
    if (_executing != executing) {
        NSString *key = NSStringFromSelector(@selector(isExecuting));
        [self willChangeValueForKey:key];
        if (executing) {
            [[APPSGlobalDataContext sharedInstance] incrementActivityIndicatorCounter];
            [self main];
        } else {
            [[APPSGlobalDataContext sharedInstance] decrementActivityIndicatorCounter];
        }
        _executing = executing;
        [self didChangeValueForKey:key];
    }
}

- (void)setFinished:(BOOL)finished {
    if (_finished != finished) {
        NSString *key = NSStringFromSelector(@selector(isFinished));
        [self willChangeValueForKey:key];
        if (finished) {
            self.executing = NO;
        }
        _finished = finished;
        [self didChangeValueForKey:key];
        if (finished && self.completionBlock != NULL) {
            self.completionBlock();
        }
    }
}

- (BOOL)isAsynchronous {
    return YES;
}

- (void)start {
    [self.scheduler schedule:^{
        if (self.cancelled) {
            self.finished = YES;
        } else {
            self.executing = YES;
        }
    }];
}

@end
