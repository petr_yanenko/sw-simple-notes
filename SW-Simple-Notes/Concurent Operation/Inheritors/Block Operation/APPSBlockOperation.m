//
//  APPSBlockOperation.m
//  TRAVERSE
//
//  Created by Petr Yanenko on 9/4/15.
//  Copyright (c) 2015 Applikey Solutions. All rights reserved.
//

#import "APPSBlockOperation.h"
#import "APPSErrorHandler.h"
#import "NSObject+Introspection.h"

@interface APPSBlockOperation ()

@property (assign, nonatomic) CGFloat delta;
@property (copy, nonatomic) void (^block)(void);

@end

@implementation APPSBlockOperation

- (instancetype)initWithTimeDelta:(CGFloat)delta block:(void (^)(void))block {
    self = [super init];
    if (self) {
        _delta = delta;
        _block = block;
    }
    return self;
}

- (void)setFinished:(BOOL)finished {
    [super setFinished:finished];
    self.block = NULL;
}

- (void)main {
    [self.scheduler afterDelay:self.delta schedule:^{
        @try {
            if (!self.cancelled) {
                self.block();
            }
        } @catch (NSException *exception) {
            [ErrorHandler handleException:exception];
        } @finally {
            self.finished = YES;
        }
    }];
}

@end
