//
//  APPSBlockOperation.h
//  TRAVERSE
//
//  Created by Petr Yanenko on 9/4/15.
//  Copyright (c) 2015 Applikey Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "APPSConcurrentOperation.h"

@interface APPSBlockOperation : APPSConcurrentOperation

- (instancetype)initWithTimeDelta:(CGFloat)delta block:(void (^)(void))block;

@end
