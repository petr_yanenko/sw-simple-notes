//
//  APPSConcurrentOperation.h
//  TRAVERSE
//
//  Created by Petr Yanenko on 9/4/15.
//  Copyright (c) 2015 Applikey Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ReactiveCocoa/ReactiveCocoa.h>

@interface APPSConcurrentOperation : NSOperation

@property (strong, readonly) RACScheduler *scheduler;

@end

@interface APPSConcurrentOperation (Protected)

- (void)setFinished:(BOOL)finished;
- (void)setExecuting:(BOOL)executing;

@end
