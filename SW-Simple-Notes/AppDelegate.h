//
//  AppDelegate.h
//  SW-Simple-Notes
//
//  Created by Petr Yanenko on 8/12/16.
//  Copyright © 2016 Petr Yanenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

