//
//  APPSDBFolder+CoreDataProperties.h
//  SW-Simple-Notes
//
//  Created by Petr Yanenko on 9/21/16.
//  Copyright © 2016 Petr Yanenko. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "APPSDBFolder.h"

NS_ASSUME_NONNULL_BEGIN

@interface APPSDBFolder (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *title;
@property (nullable, nonatomic, retain) NSSet<APPSDBNote *> *notes;

@end

@interface APPSDBFolder (CoreDataGeneratedAccessors)

- (void)addNotesObject:(APPSDBNote *)value;
- (void)removeNotesObject:(APPSDBNote *)value;
- (void)addNotes:(NSSet<APPSDBNote *> *)values;
- (void)removeNotes:(NSSet<APPSDBNote *> *)values;

@end

NS_ASSUME_NONNULL_END
