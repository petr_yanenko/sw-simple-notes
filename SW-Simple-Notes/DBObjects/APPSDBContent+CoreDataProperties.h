//
//  APPSDBContent+CoreDataProperties.h
//  SW-Simple-Notes
//
//  Created by Petr Yanenko on 9/21/16.
//  Copyright © 2016 Petr Yanenko. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "APPSDBContent.h"

NS_ASSUME_NONNULL_BEGIN

@interface APPSDBContent (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *content;
@property (nullable, nonatomic, retain) APPSDBNote *note;

@end

NS_ASSUME_NONNULL_END
