//
//  APPSDBFolder.h
//  SW-Simple-Notes
//
//  Created by Petr Yanenko on 9/21/16.
//  Copyright © 2016 Petr Yanenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "APPSDBObject.h"

@class APPSDBNote;

NS_ASSUME_NONNULL_BEGIN

@interface APPSDBFolder : APPSDBObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "APPSDBFolder+CoreDataProperties.h"
