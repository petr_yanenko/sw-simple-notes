//
//  APPSDBContent+CoreDataProperties.m
//  SW-Simple-Notes
//
//  Created by Petr Yanenko on 9/21/16.
//  Copyright © 2016 Petr Yanenko. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "APPSDBContent+CoreDataProperties.h"

@implementation APPSDBContent (CoreDataProperties)

@dynamic content;
@dynamic note;

@end
