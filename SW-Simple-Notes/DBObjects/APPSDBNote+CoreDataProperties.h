//
//  APPSDBNote+CoreDataProperties.h
//  SW-Simple-Notes
//
//  Created by Petr Yanenko on 9/21/16.
//  Copyright © 2016 Petr Yanenko. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "APPSDBNote.h"

NS_ASSUME_NONNULL_BEGIN

@interface APPSDBNote (CoreDataProperties)

@property (nonatomic) NSTimeInterval lastEdited;
@property (nullable, nonatomic, retain) NSData *content;
@property (nullable, nonatomic, retain) APPSDBFolder *folder;

@end

@interface APPSDBNote (CoreDataGeneratedAccessors)

- (void)addContentObject:(APPSDBContent *)value;
- (void)removeContentObject:(APPSDBContent *)value;
- (void)addContent:(NSSet<APPSDBContent *> *)values;
- (void)removeContent:(NSSet<APPSDBContent *> *)values;

@end

NS_ASSUME_NONNULL_END
