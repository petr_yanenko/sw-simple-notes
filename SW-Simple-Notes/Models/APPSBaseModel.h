//
//  APPSBaseModel.h
//  Simple Notes
//
//  Created by Petr Yanenko on 8/6/16.
//  Copyright © 2016 Petr Yanenko. All rights reserved.
//

#import "APPSBaseSubject.h"
#import "APPSModel.h"

@interface APPSBaseModel<T> : APPSBaseSubject <APPSModel> {

@protected
    T _data;
}

@property (assign, nonatomic, readonly) BOOL loading;
@property (assign, nonatomic, readonly) BOOL newData;
@property (strong, nonatomic, readonly) NSString *errorReason;

@end

#pragma mark protected
@interface APPSBaseModel ()

@property (assign, nonatomic) BOOL loading;
@property (assign, nonatomic) BOOL newData;
@property (strong, nonatomic) NSString *errorReason;

- (void)load;

@end
