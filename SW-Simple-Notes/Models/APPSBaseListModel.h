//
//  APPSListModel.h
//  Simple Notes
//
//  Created by Petr Yanenko on 8/6/16.
//  Copyright © 2016 Petr Yanenko. All rights reserved.
//

#import "APPSBaseModel.h"
#import "APPSListModel.h"

@class APPSObject;

@interface APPSBaseListModel<T, U> : APPSBaseModel<T> <APPSListModel>

@property (strong, nonatomic, readonly) U selectedItem;

- (U)itemAtIndex:(NSInteger)index;

@end

#pragma mark protected
@interface APPSBaseListModel ()

@property (assign, nonatomic, readwrite) BOOL itemIsSelected;

@end
