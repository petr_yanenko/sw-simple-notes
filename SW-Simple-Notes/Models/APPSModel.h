//
//  APPSModel.h
//  Simple Notes
//
//  Created by Petr Yanenko on 8/6/16.
//  Copyright © 2016 Petr Yanenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol APPSModel <APPSObserverProtocol>

@property (assign, nonatomic, readonly) BOOL loading;
@property (assign, nonatomic, readonly) BOOL newData;
@property (strong, nonatomic, readonly) NSString *errorReason;

- (void)reset;
- (void)loadData;
- (void)cancel;

@end
