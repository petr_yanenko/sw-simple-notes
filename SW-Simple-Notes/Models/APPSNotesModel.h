//
//  APPSNotesModel.h
//  Simple Notes
//
//  Created by Petr Yanenko on 8/6/16.
//  Copyright © 2016 Petr Yanenko. All rights reserved.
//

#import "APPSNotesListModel.h"

@class APPSFolder, APPSNote, APPSContentModel;
@class APPSFoldersModel;

@interface APPSNotesModel : APPSNotesListModel <NSArray<APPSNote *> *, APPSNote *>

@property (strong, nonatomic, readonly) APPSFolder *selectedFolder;
@property (strong, nonatomic, readonly) APPSContentModel *contentModel;
@property (weak, nonatomic, readonly) APPSFoldersModel *foldersModel;

- (instancetype)initWithDBManager:(APPSDBManager *)manager foldersModel:(APPSFoldersModel *)model;

- (void)insertNote;
- (BOOL)deleteNoteResources:(APPSObject *)note;

@end
