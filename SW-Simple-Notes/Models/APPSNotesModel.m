//
//  APPSNotesModel.m
//  Simple Notes
//
//  Created by Petr Yanenko on 8/6/16.
//  Copyright © 2016 Petr Yanenko. All rights reserved.
//

#import "APPSNotesModel.h"
#import "APPSFolder.h"
#import "APPSNote.h"
#import "APPSDBManager.h"
#import "NSArray+Finding.h"
#import "APPSContentModel.h"
#import "Constants.h"
#import "NSFileManager+Customizing.h"
#import "APPSFoldersModel.h"


@interface APPSNotesModel ()

@property (strong, nonatomic, readwrite) APPSContentModel *contentModel;

@end

@implementation APPSNotesModel

- (APPSFolder *)selectedFolder {
    return [self.foldersModel.dataArray apps_findSelected];
}

- (instancetype)initWithDBManager:(APPSDBManager *)manager foldersModel:(APPSFoldersModel *)model {
    self = [super initWithDBManager:manager];
    if (self) {
        _foldersModel = model;
        _contentModel = [[APPSContentModel alloc] initWithDBManager:manager notesModel:self];
    }
    return self;
}

- (NSArray *)dataArray {
    return [self selectedFolder].notes;
}

- (void)load {
    _data = self.dataArray;
    [super load];
}

- (void)reset {
    _data = nil;
    self.selectedFolder.notes = nil;
}

- (void)insertNote {
    APPSFolder *selectedFolder = [self selectedFolder];
    [self saveWithBlock:^{
        [_dbManager addNoteToFolder:selectedFolder.objectID];
    } success:^{
        if (selectedFolder.notes.count == 1) {
            [self selectItem:selectedFolder.notes.firstObject.objectID];
        }
    }];
}

- (void)deleteItem:(NSString *)objectID {
    APPSFolder *selectedFolder = [self selectedFolder];
    APPSObject *object = [selectedFolder.notes apps_findObject:objectID];
    BOOL resources = [self deleteNoteResources:object];
    if (resources) {
        [self saveWithBlock:^{
            [object deleteObject];
        } success:NULL];

        [super deleteItem:objectID];
    }
}

- (void)resetChildModel {
    [self.contentModel reset];
}

- (BOOL)deleteNoteResources:(APPSObject *)note {
    NSString *path = [[[APPSConstants sharedInstance] resourceDirectory] stringByAppendingPathComponent:note.percentEncodedObjectID];
    return [[NSFileManager defaultManager] apps_deleteItemAtPath:path];
}

- (void)saveWithBlock:(void (^)())block success:(void (^)())success {
    if ([_dbManager saveWithBlock:^{
        if (block) {
            block();
        }
        self.selectedFolder.notes = nil;
        [self.selectedFolder updateObject];
    }]) {
        self.newData = YES;
        if (success) {
            success();
        }
    }
}

@end
