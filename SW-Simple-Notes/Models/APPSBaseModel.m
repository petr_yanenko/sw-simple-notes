//
//  APPSBaseModel.m
//  Simple Notes
//
//  Created by Petr Yanenko on 8/6/16.
//  Copyright © 2016 Petr Yanenko. All rights reserved.
//

#import "APPSBaseModel.h"
#import "APPSGlobalDataContext.h"

@interface APPSBaseModel ()

@end

@implementation APPSBaseModel

- (void)setLoading:(BOOL)loading {
    if (_loading != loading) {
        _loading = loading;
        if (loading) {
            [[APPSGlobalDataContext sharedInstance] incrementActivityIndicatorCounter];
        } else {
            [[APPSGlobalDataContext sharedInstance] decrementActivityIndicatorCounter];
        }
    }
}

- (instancetype)init {
    self = [super init];
    if (self) {
        [self addStateSignalForProperty:@selector(loading)];
        [self addStateSignalForProperty:@selector(errorReason)];
        [self addStreamSignalForProperty:@selector(newData)];
    }
    return self;
}

- (void)reset {
    
}

- (void)loadData {
    self.loading = YES;
    [self load];
    self.loading = NO;
}

- (void)cancel {
    
}

- (void)load {
    
}

@end
