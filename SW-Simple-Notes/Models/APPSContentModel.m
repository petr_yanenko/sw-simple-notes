//
// Created by Petr Yanenko on 8/17/16.
// Copyright (c) 2016 Petr Yanenko. All rights reserved.
//

#import "APPSContentModel.h"
#import "APPSFolder.h"
#import "APPSNote.h"
#import "APPSErrorHandler.h"
#import "NSObject+Introspection.h"
#import "Constants.h"
#import "APPSDBManager.h"
#import "NSFileManager+Customizing.h"
#import "APPSNotesModel.h"

@interface APPSContentModel ()

@property (strong, nonatomic) APPSDBManager *dbManager;

@end

@implementation APPSContentModel

- (APPSNote *)selectedNote {
    return self.notesModel.selectedItem;
}

- (instancetype)initWithDBManager:(APPSDBManager *)manager notesModel:(APPSNotesModel *)notesModel {
    self = [super init];
    if (self) {
        _dbManager = manager;
        _notesModel = notesModel;
    }
    return self;
}

- (void)insertNotePartWithIndex:(NSInteger)index
                       location:(NSInteger)location
                         length:(NSInteger)length
                        content:(NSString *)content
                           type:(NSString *)type {
    if (self.selectedNote != nil) {
        NSString *data = [self createNotePartWithContent:content type:type];
        if ([type isEqualToString:kTextCode]) {
            self.selectedNote.content = [self insertNotePart:data atIndex:index inArray:self.selectedNote.content];
        } else if ([type isEqualToString:kImageCode] || [type isEqualToString:kVideoCode]) {
            NSInteger newLocation = location;
            NSInteger newIndex = index;
            [self changeNotePartWithIndex:&newIndex location:&newLocation length:length text:@"" note:self.selectedNote];
            NSInteger insertedIndex = newIndex;
            if (newLocation > 0) {
                NSString *currentNotePart = self.selectedNote.content[newIndex];
                NSString *currentType = [currentNotePart substringToIndex:kTypeCodeLength];
                if ([currentType isEqualToString:kTextCode]) {
                    NSString *currentContent = [currentNotePart substringFromIndex:kTypeLength];
                    if (newLocation < currentContent.length) {
                        [self splitNotePartWithIndex:newIndex location:newLocation note:self.selectedNote];
                    }
                }
                insertedIndex++;
            }
            self.selectedNote.content = [self insertNotePart:data atIndex:insertedIndex inArray:self.selectedNote.content];
        } else {
            [[APPSErrorHandler sharedInstance] handleErrorInDomain:self.apps_className
                                                          withCode:0
                                                       description:@"Unknown note part type"];
        }
        [self notePartDidChange];
    }
}

- (void)changeNotePartWithIndex:(NSInteger)index
                       location:(NSInteger)location
                         length:(NSInteger)length
                           text:(NSString *)text {
    if (self.selectedNote != nil) {
        if ([self changeNotePartWithIndex:&index
                                 location:&location
                                   length:length
                                     text:text
                                     note:self.selectedNote]) {
            [self notePartDidChange];
        }
    }
}

- (NSString *)mediaPathWithContent:(NSString *)content aspectRatio:(CGFloat *)aspectRatio {
    NSInteger pathStartIndex = [content rangeOfString:[[APPSConstants sharedInstance] attachmentDivider] ].location;
    *aspectRatio = [[content substringToIndex:pathStartIndex] floatValue];
    NSString *path = [content substringFromIndex:pathStartIndex + 1];
    return path;
}

- (NSString *)fullFilePath:(APPSObject *)note {
    NSFileManager *manager = [NSFileManager defaultManager];
    NSString *directoryPath = [[[APPSConstants sharedInstance] resourceDirectory] stringByAppendingPathComponent:note.percentEncodedObjectID];
    if (![manager fileExistsAtPath:directoryPath]) {
        NSError *error;
        if (![manager createDirectoryAtPath:directoryPath
           withIntermediateDirectories:YES
                            attributes:nil
                                 error:&error]) {
            [ErrorHandler handleErrorInDomain:self.apps_className withCode:2 description:@"Resource directory was not created"];
            return nil;
        }
    }
    NSDirectoryEnumerator *enumerator = [manager enumeratorAtPath:directoryPath];
    NSInteger lastFileNumber = 0;
    NSString *file;
    while((file = [enumerator nextObject])) {
        NSInteger currentNumber = [file lastPathComponent].integerValue;
        if (currentNumber > lastFileNumber) {
            lastFileNumber = currentNumber;
        }
    }
    NSString *fileName = [[NSString alloc] initWithFormat:@"%ld", (long)++lastFileNumber];
    return [directoryPath stringByAppendingPathComponent:fileName];
}

- (void)insertNewNote {
    [self.notesModel insertNote];
    __block APPSNote *lastNote;
    [self.notesModel.selectedFolder.notes enumerateObjectsUsingBlock:^(APPSNote * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (lastNote == nil || [lastNote.lastEdited compare:obj.lastEdited] == NSOrderedAscending) {
            lastNote = obj;
        }
    }];
    [self.notesModel selectItem:lastNote.objectID];
}

- (void)notePartDidChange {
    if ([self.dbManager saveWithBlock:^{
        [self.selectedNote updateContent];
    }]) {
        self.newData = YES;
    }
}

- (NSString *)createNotePartWithContent:(NSString *)content type:(NSString *)type {
    NSString *divider = @"//";
    return [[NSString alloc] initWithFormat:@"%@%@%@", type, divider, content];
}

- (BOOL)changeNotePartWithIndex:(NSInteger *)index
                       location:(NSInteger *)location
                         length:(NSInteger)length
                           text:(NSString *)text
                           note:(APPSNote *)note {
    if (*index < note.content.count) {
        NSString *notePart = note.content[*index];
        NSString *insertedText = text;
        NSInteger contentIndex = *location + kTypeLength;
        NSString *type = [notePart substringToIndex:kTypeCodeLength];
        NSInteger remainderLength = [type isEqualToString:kTextCode] ?
                (NSInteger)notePart.length - contentIndex : [[APPSConstants sharedInstance] attachmentLength] - *location;

        BOOL lessThanLength = remainderLength < length;
        NSInteger removedLength = [self notePartRemovedLengthWithLocation:*location
                                                                   length:length
                                                          remainderLength:remainderLength
                                                           lessThanLength:lessThanLength
                                                                     type:type
                                                                 notePart:notePart];

        NSInteger newIndex = *index;
        NSInteger nextIndex = *index + 1;
        NSInteger nextLocation = 0, newLocation = *location;
        [self changeContentWithNewIndex:&newIndex
                                   note:note
                              nextIndex:&nextIndex
                           nextLocation:&nextLocation
                               notePart:notePart
                                   type:type
                           contentIndex:contentIndex
                               location:&newLocation
                          removedLength:removedLength
                           insertedText:insertedText];

        if (lessThanLength) {
            NSInteger nativeNextLocation = nextLocation;
            [self changeNotePartWithIndex:&nextIndex location:&nativeNextLocation length:length - remainderLength text:@"" note:note];
        }
        *index = newIndex;
        *location = newLocation;
        return YES;
    }
    return NO;
}

- (NSInteger)notePartRemovedLengthWithLocation:(NSInteger)location
                                        length:(NSInteger)length
                               remainderLength:(NSInteger)remainderLength
                                lessThanLength:(BOOL)lessThanLength
                                          type:(NSString *)type
                                      notePart:(NSString *)notePart {
    NSInteger removedLength = 0;
    if (![type isEqualToString:kTextCode]) {
        if (length >= [[APPSConstants sharedInstance] attachmentLength]) {
            removedLength = notePart.length - kTypeLength;
        }
    } else if (lessThanLength) {
        removedLength = remainderLength;
    } else {
        removedLength = length;
    }
    return removedLength;
}

- (void)changeContentWithNewIndex:(NSInteger *)newIndex
                             note:(APPSNote *)note
                        nextIndex:(NSInteger *)nextIndex
                     nextLocation:(NSInteger *)nextLocation
                         notePart:(NSString *)notePart
                             type:(NSString *)type
                     contentIndex:(NSInteger)contentIndex
                         location:(NSInteger *)location
                    removedLength:(NSInteger)removedLength
                     insertedText:(NSString *)insertedText {
    NSMutableString *mutableChangedNotePart = [notePart mutableCopy];
    [mutableChangedNotePart deleteCharactersInRange:NSMakeRange(contentIndex, removedLength)];
    NSString *changedNotePart = [self insertTextWithChangedNotePart:[mutableChangedNotePart copy]
                                                       contentIndex:contentIndex
                                                     originLocation:*location
                                                       insertedText:insertedText
                                                           notePart:notePart
                                                               type:type
                                                      removedLength:removedLength
                                                               note:note
                                                           newIndex:newIndex
                                                          nextIndex:nextIndex
                                                       nextLocation:nextLocation];
    NSInteger previousContentLength = [self replaceOldContentWithNewIndex:*newIndex
                                                                     note:note
                                                                nextIndex:nextIndex
                                                             nextLocation:nextLocation
                                                                 notePart:notePart
                                                                     type:type
                                                          changedNotePart:changedNotePart];
    *location += previousContentLength;
    if (previousContentLength > 0) {
        (*newIndex)--;
    }

}

- (NSInteger)replaceOldContentWithNewIndex:(NSInteger)newIndex
                                 note:(APPSNote *)note
                            nextIndex:(NSInteger *)nextIndex
                         nextLocation:(NSInteger *)nextLocation
                             notePart:(NSString *)notePart
                                 type:(NSString *)type
                      changedNotePart:(NSString *)changedNotePart {
    NSInteger previousContentLength;
    NSMutableArray *mutableNoteContent = [note.content mutableCopy];
    [mutableNoteContent removeObjectAtIndex:newIndex];
    note.content = [mutableNoteContent copy];
    if (changedNotePart.length - kTypeLength > 0) {
        [mutableNoteContent insertObject:changedNotePart atIndex:newIndex];
        note.content = [mutableNoteContent copy];
        previousContentLength = [self checkOnMutatingWithIndex:newIndex
                                                          note:note
                                                     nextIndex:nextIndex
                                                  nextLocation:nextLocation
                                               changedNotePart:changedNotePart
                                                          type:type];
    } else {
        (*nextIndex)--;
        [self deleteResourcesWithNotePart:notePart];
        previousContentLength = [self checkOnMergeWithIndex:newIndex note:note nextIndex:nextIndex nextLocation:nextLocation];
    }
    return previousContentLength;
}

- (NSInteger)checkOnMutatingWithIndex:(NSInteger)index
                                 note:(APPSNote *)note
                            nextIndex:(NSInteger *)nextIndex
                         nextLocation:(NSInteger *)nextLocation
                      changedNotePart:(NSString *)changedNotePart
                                 type:(NSString *)type {
    NSInteger previousContentLength = 0;
    NSString *changedType = [changedNotePart substringToIndex:kTypeCodeLength];
    if (![changedType isEqualToString:type]) {
        previousContentLength = [self checkOnMergeToSiblingsWithIndex:index note:note nextIndex:nextIndex nextLocation:nextLocation];
    }
    return previousContentLength;
}

- (NSInteger)checkOnMergeToSiblingsWithIndex:(NSInteger)index
                                        note:(APPSNote *)note
                                   nextIndex:(NSInteger *)nextIndex
                                nextLocation:(NSInteger *)nextLocation {
    [self checkOnMergeWithIndex:index + 1 note:note nextIndex:nextIndex nextLocation:nextLocation];
    NSInteger previousContentLength = [self checkOnMergeWithIndex:index note:note nextIndex:nextIndex nextLocation:nextLocation];
    return previousContentLength;
}

- (NSArray *)insertNotePart:(NSString *)notePart atIndex:(NSInteger)index inArray:(NSArray *)array {
    NSMutableArray *mutableArray = [array mutableCopy];
    [mutableArray insertObject:notePart atIndex:index];
    return [mutableArray copy];
}

- (NSString *)insertTextWithChangedNotePart:(NSString *)changedNotePart
                                   location:(NSInteger)location
                               insertedText:(NSString *)insertedText {
    NSMutableString *mutablePart = [changedNotePart mutableCopy];
    [mutablePart insertString:insertedText atIndex:location];
    return [mutablePart copy];
}

- (NSString *)replaceMediaPartWithChangedNotePart:(NSString *)changedNotePart
                                         location:(NSInteger)location
                                     insertedText:(NSString *)insertedText
                                         notePart:(NSString *)notePart {
    changedNotePart = [self insertTextWithChangedNotePart:changedNotePart location:location insertedText:insertedText];
    NSMutableString *mutablePart = [changedNotePart mutableCopy];
    [mutablePart deleteCharactersInRange:NSMakeRange(0, kTypeCodeLength)];
    [mutablePart insertString:kTextCode atIndex:0];
    [self deleteResourcesWithNotePart:notePart];
    return [mutablePart copy];
}

- (NSString *)insertTextWithChangedNotePart:(NSString *)changedNotePart
                               contentIndex:(NSInteger)contentIndex
                             originLocation:(NSInteger)originLocation
                               insertedText:(NSString *)insertedText
                                   notePart:(NSString *)notePart
                                       type:(NSString *)type
                              removedLength:(NSInteger)removedLength
                                       note:(APPSNote *)note
                                   newIndex:(NSInteger *)newIndex
                                  nextIndex:(NSInteger *)nextIndex
                               nextLocation:(NSInteger *)nextLocation {
    if (insertedText.length > 0) {
        if ([type isEqualToString:kTextCode]) {
            changedNotePart = [self insertTextWithChangedNotePart:changedNotePart
                                                         location:contentIndex
                                                     insertedText:insertedText];
        } else if (removedLength > 0) {
            changedNotePart = [self replaceMediaPartWithChangedNotePart:changedNotePart
                                                               location:0 + kTypeLength
                                                           insertedText:insertedText
                                                               notePart:notePart];
        } else {
            [self insertTextPartWithText:insertedText
                                location:originLocation
                                    note:note
                                newIndex:newIndex
                               nextIndex:nextIndex
                            nextLocation:nextLocation];
        }
    }
    return changedNotePart;
}

- (void)insertTextPartWithText:(NSString *)insertedText
                      location:(NSInteger)location
                          note:(APPSNote *)note
                      newIndex:(NSInteger *)newIndex
                     nextIndex:(NSInteger *)nextIndex
                  nextLocation:(NSInteger *)nextLocation {
    NSString *data = [self createNotePartWithContent:insertedText type:kTextCode];
    NSInteger insertIndex, mergeIndex;
    BOOL insertBefore = location == 0;
    if (insertBefore) {
        insertIndex = mergeIndex = *newIndex;
        (*newIndex)++;
        (*nextIndex)++;
    } else {
        insertIndex = *newIndex + 1;
        mergeIndex = insertIndex + 1;
        if (nextIndex > newIndex) {
            (*nextIndex)++;
        }
    }
    self.selectedNote.content = [self insertNotePart:data atIndex:insertIndex inArray:self.selectedNote.content];
    NSInteger beforeMerge = *nextIndex;
    [self checkOnMergeWithIndex:mergeIndex note:note nextIndex:nextIndex nextLocation:nextLocation];
    NSInteger afterMerge = *nextIndex;
    if (insertBefore) {
        *newIndex -= beforeMerge - afterMerge;
    }
}

- (NSInteger)checkOnMergeWithIndex:(NSInteger)index
                         note:(APPSNote *)note
                    nextIndex:(NSInteger *)nextIndex
                 nextLocation:(NSInteger *)nextLocation {
    if (index > 0 && index < note.content.count) {
        NSInteger previousIndex = index - 1;
        NSString *previousPart = note.content[previousIndex];
        NSString *nextPart = note.content[index];
        NSString *previousType = [previousPart substringToIndex:kTypeCodeLength];
        NSString *nextType = [nextPart substringToIndex:kTypeCodeLength];
        if ([previousType isEqualToString:nextType] && [previousType isEqualToString:kTextCode]) {
            [self mergeNotePartsWithIndex:previousIndex note:note];
            NSString *previousContent = [previousPart substringFromIndex:kTypeLength];
            if (index == *nextIndex) {
                *nextLocation += previousContent.length;
            }
            (*nextIndex)--;
            return previousContent.length;
        }
    }
    return 0;
}

- (void)mergeNotePartsWithIndex:(NSInteger)firstIndex note:(APPSNote *)note {
    NSString *firstPart = note.content[firstIndex];
    NSString *secondPart = note.content[firstIndex + 1];
    NSMutableArray *mutableNoteContent = [note.content mutableCopy];
    [mutableNoteContent removeObject:firstPart];
    [mutableNoteContent removeObject:secondPart];
    NSString *secondContent = [secondPart substringFromIndex:kTypeLength];
    NSString *mergedPart = [firstPart stringByAppendingString:secondContent];
    [mutableNoteContent insertObject:mergedPart atIndex:firstIndex];
    note.content = [mutableNoteContent copy];
}

- (void)splitNotePartWithIndex:(NSInteger)index location:(NSInteger)location note:(APPSNote *)note {
    NSString *part = note.content[index];
    NSInteger contentIndex = location + kTypeLength;
    NSString *firstPart = [part substringToIndex:contentIndex];
    NSString *typePart = [part substringToIndex:kTypeLength];
    NSString *secondPart = [typePart stringByAppendingString:[part substringFromIndex:contentIndex]];
    NSMutableArray *mutableNoteContent = [note.content mutableCopy];
    [mutableNoteContent removeObject:part];
    [mutableNoteContent insertObject:secondPart atIndex:index];
    [mutableNoteContent insertObject:firstPart atIndex:index];
    note.content = [mutableNoteContent copy];
}

- (void)deleteResourcesWithNotePart:(NSString *)notePart {
    NSString *type = [notePart substringToIndex:kTypeCodeLength];
    if (![type isEqualToString:kTextCode]) {
        NSString *content = [notePart substringFromIndex:kTypeLength];
        CGFloat aspectRatio;
        NSString *pathContent = [self mediaPathWithContent:content aspectRatio:&aspectRatio];
        NSString *path = [[[APPSConstants sharedInstance] resourceDirectory] stringByAppendingPathComponent:pathContent];
        NSString *previewPath = [path stringByAppendingPathExtension:[[APPSConstants sharedInstance] previewImageExtension]];
        NSString *imagePath = [path stringByAppendingPathExtension:[[APPSConstants sharedInstance] originImageExtension]];
        NSString *videoPath = [path stringByAppendingPathExtension:[[APPSConstants sharedInstance] originMovieExtension]];
        [self deleteFileAtPath:previewPath];
        [self deleteFileAtPath:imagePath];
        [self deleteFileAtPath:videoPath];
    }
}

- (void)deleteFileAtPath:(NSString *)path {
    NSFileManager *manager = [NSFileManager defaultManager];
    [manager apps_deleteItemAtPath:path errorDomain:[self apps_className] errorCode:1];
}

@end
