//
// Created by Petr Yanenko on 8/16/16.
// Copyright (c) 2016 Petr Yanenko. All rights reserved.
//

#import "APPSFoldersModel.h"
#import "APPSDBManager.h"
#import "NSArray+Finding.h"
#import "APPSFolder.h"
#import "APPSNote.h"
#import "APPSNotesModel.h"

@interface APPSFoldersModel ()

@property (strong, nonatomic, readwrite) APPSNotesModel *notesModel;

@end

@implementation APPSFoldersModel

- (id)dataArray {
    return _data;
}

- (instancetype)initWithDBManager:(APPSDBManager *)manager {
    self = [super initWithDBManager:manager];
    if (self) {
        _data = [[NSMutableArray alloc] init];
        _notesModel = [[APPSNotesModel alloc] initWithDBManager:manager foldersModel:self];
        @weakify(self);
        [_notesModel attachObserver:self forProperty:@selector(newData) handler:^(APPSNotesModel *subject) {
            @strongify(self);
            self.newData = YES;
        }];
    }
    return self;
}

- (void)load {
    if (_data.count > 0) {
        [_data removeAllObjects];
    }
    [_data addObjectsFromArray:[[_dbManager allFolders] mutableCopy]];
    [super load];
}

- (void)reset {
    [_data removeAllObjects];
}

- (void)insertFolderWithTitle:(NSString *)title {
    if ([_dbManager saveWithBlock:^{
        [_dbManager createFolderWithTitle:title];
    }]) {
        [self loadData];
    }
}

- (void)deleteItem:(NSString *)objectID {
    APPSFolder *object = [_data apps_findObject:objectID];
    __block BOOL resources = YES;
    [object.notes enumerateObjectsUsingBlock:^(APPSNote * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        resources = [self.notesModel deleteNoteResources:obj];
        *stop = !resources;
    }];
    if (resources) {
        if ([_dbManager saveWithBlock:^{
            [object deleteObject];
        }]) {
            [self loadData];
        }

        [super deleteItem:objectID];
    }
}

- (void)resetChildModel {
    [self.notesModel reset];
    [self.notesModel resetChildModel];
}

@end
