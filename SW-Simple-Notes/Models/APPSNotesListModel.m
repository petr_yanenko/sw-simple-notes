//
// Created by Petr Yanenko on 8/18/16.
// Copyright (c) 2016 Petr Yanenko. All rights reserved.
//

#import "APPSNotesListModel.h"
#import "APPSDBManager.h"
#import "NSArray+Finding.h"
#import "APPSObject.h"


@implementation APPSNotesListModel

- (instancetype)initWithDBManager:(APPSDBManager *)manager {
    self = [super init];
    if (self) {
        _dbManager = manager;
    }
    return self;
}

- (NSInteger)count {
    return self.dataArray.count;
}

- (APPSFolder *)selectedItem {
    return [self.dataArray apps_findSelected];
}

- (NSArray *)dataArray {
    return nil;
}

- (void)load {
    if (_data != nil) {
        self.newData = YES;
    }
}

- (void)selectItem:(NSString *)objectID {
    APPSObject *selectedObject = [self.dataArray apps_findObject:objectID];
    if (selectedObject != nil) {
        APPSObject *oldSelectedObject = [self.dataArray apps_findSelected];
        oldSelectedObject.selected = NO;
        selectedObject.selected = YES;
        if ([_dbManager saveWithBlock:^{
            [oldSelectedObject updateObject];
            [selectedObject updateObject];
        }]) {
            [self resetChildModel];
            self.itemIsSelected = YES;
        } else {
            selectedObject.selected = !(oldSelectedObject.selected = YES);
        }
    } else {
        self.itemIsSelected = NO;
    }
}

- (void)deleteItem:(NSString *)objectID {
    if ([self.dataArray apps_findSelected] == nil) {
        [self resetChildModel];
        [self selectItem:nil];
    }
}

- (void)resetChildModel {
    
}

- (APPSFolder *)itemAtIndex:(NSInteger)index {
    return self.dataArray [index];
}

@end
