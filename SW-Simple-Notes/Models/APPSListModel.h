//
//  APPSListModel.h
//  SW-Simple-Notes
//
//  Created by Petr Yanenko on 12/4/16.
//  Copyright © 2016 Petr Yanenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol APPSListModel <APPSModel>

@property (assign, nonatomic, readonly) NSInteger count;
@property (assign, nonatomic, readonly) BOOL itemIsSelected;
@property (strong, nonatomic, readonly) id selectedItem;

- (void)selectItem:(NSString *)objectID;
- (id)itemAtIndex:(NSInteger)index;
- (void)deleteItem:(NSString *)objectID;

@end
