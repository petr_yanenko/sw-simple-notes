//
// Created by Petr Yanenko on 8/18/16.
// Copyright (c) 2016 Petr Yanenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "APPSBaseListModel.h"

@class APPSDBManager;


@interface APPSNotesListModel<T, U> : APPSBaseListModel<T, U> {

@protected
    APPSDBManager *_dbManager;

}

@property (strong, nonatomic, readonly) NSArray *dataArray;

- (instancetype)initWithDBManager:(APPSDBManager *)manager;

- (void)resetChildModel;

@end