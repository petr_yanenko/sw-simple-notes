//
// Created by Petr Yanenko on 8/17/16.
// Copyright (c) 2016 Petr Yanenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "APPSBaseListModel.h"

@class APPSNote;
@class APPSDBManager;
@class APPSNotesModel;


@interface APPSContentModel : APPSBaseListModel <NSArray<NSString *> *, NSString *>

@property (assign, nonatomic) NSInteger changingLocation;
@property (strong, nonatomic, readonly) APPSNote *selectedNote;
@property (weak, nonatomic, readonly) APPSNotesModel *notesModel;

- (instancetype)initWithDBManager:(APPSDBManager *)manager notesModel:(APPSNotesModel *)notesModel;

- (void)insertNotePartWithIndex:(NSInteger)index
                       location:(NSInteger)location
                         length:(NSInteger)length
                        content:(NSString *)content
                           type:(NSString *)type;

- (void)changeNotePartWithIndex:(NSInteger)index
                       location:(NSInteger)location
                         length:(NSInteger)length
                           text:(NSString *)text;

- (NSString *)mediaPathWithContent:(NSString *)content aspectRatio:(CGFloat *)aspectRatio;
- (NSString *)fullFilePath:(APPSObject *)note;
- (void)insertNewNote;

@end
