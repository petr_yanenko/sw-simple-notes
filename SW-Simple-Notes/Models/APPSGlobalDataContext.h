//
//  APPSGlobalDataContext.h
//  SW-Simple-Notes
//
//  Created by Petr Yanenko on 9/9/16.
//  Copyright © 2016 Petr Yanenko. All rights reserved.
//

#import "APPSBaseSubject.h"

@interface APPSGlobalDataContext : APPSBaseSubject

@property (assign, nonatomic, readonly) NSInteger activityIndicatorCounter;

+ (instancetype)sharedInstance;

- (void)incrementActivityIndicatorCounter;
- (void)decrementActivityIndicatorCounter;

@end
