//
//  APPSListModel.m
//  Simple Notes
//
//  Created by Petr Yanenko on 8/6/16.
//  Copyright © 2016 Petr Yanenko. All rights reserved.
//

#import "APPSBaseListModel.h"

@implementation APPSBaseListModel

@synthesize count = _count;
@synthesize itemIsSelected = _itemIsSelected;
@synthesize selectedItem = _selectedItem;

- (instancetype)init {
    self = [super init];
    if (self) {
        [self addStreamSignalForProperty:@selector(itemIsSelected)];
    }
    return self;
}

- (void)selectItem:(NSString *)objectID {
    
}

- (id)itemAtIndex:(NSInteger)index {
    return nil;
}

- (void)deleteItem:(NSString *)objectID {
    
}

@end
