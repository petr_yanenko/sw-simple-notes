//
// Created by Petr Yanenko on 8/16/16.
// Copyright (c) 2016 Petr Yanenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "APPSNotesListModel.h"

@class APPSFolder, APPSNotesModel;

@interface APPSFoldersModel : APPSNotesListModel <NSMutableArray<APPSFolder *> *, APPSFolder *>

@property (strong, nonatomic, readonly) APPSNotesModel *notesModel;

- (void)insertFolderWithTitle:(NSString *)title;

@end