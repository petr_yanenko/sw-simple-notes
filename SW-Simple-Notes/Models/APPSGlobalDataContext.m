//
//  APPSGlobalDataContext.m
//  SW-Simple-Notes
//
//  Created by Petr Yanenko on 9/9/16.
//  Copyright © 2016 Petr Yanenko. All rights reserved.
//

#import "APPSGlobalDataContext.h"
#import "APPSErrorHandler.h"
#import "NSObject+Introspection.h"

@interface APPSGlobalDataContext ()

@property (assign, nonatomic, readwrite) NSInteger activityIndicatorCounter;

@end

@implementation APPSGlobalDataContext {
    NSObject *_activityIndicatorCounterLock;
}

@synthesize activityIndicatorCounter = _activityIndicatorCounter;

- (NSInteger)activityIndicatorCounter {
    @synchronized (_activityIndicatorCounterLock) {
        return _activityIndicatorCounter;
    }
}

- (void)setActivityIndicatorCounter:(NSInteger)activityIndicatorCounter {
    @synchronized (_activityIndicatorCounterLock) {
        if (_activityIndicatorCounter != activityIndicatorCounter) {
            _activityIndicatorCounter = activityIndicatorCounter;
        }
    }
}

+ (instancetype)sharedInstance {
    static APPSGlobalDataContext *instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[APPSGlobalDataContext alloc] init];
    });
    return instance;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        _activityIndicatorCounterLock = [[NSObject alloc] init];
        [self addStateSignalForProperty:@selector(activityIndicatorCounter)];
    }
    return self;
}

- (void)incrementActivityIndicatorCounter {
    @synchronized (_activityIndicatorCounterLock) {
        self.activityIndicatorCounter++;
    }
}

- (void)decrementActivityIndicatorCounter {
    @synchronized (_activityIndicatorCounterLock) {
        if (_activityIndicatorCounter > 0) {
            self.activityIndicatorCounter--;
        } else {
            [ErrorHandler handleErrorInDomain:self.apps_className withCode:0 description:@"Unbalaced decrement of activity indicator counter"];
        }
    }
}

@end
