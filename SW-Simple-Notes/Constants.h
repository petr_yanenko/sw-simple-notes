
#import <UIKit/UIKit.h>

@interface APPSConstants : NSObject

+ (_Nonnull instancetype)sharedInstance;

@property (nonatomic, readonly, nonnull) NSString *documentsDirectory;
@property (nonatomic, readonly, nonnull) NSString *tempDirectory;
@property (nonatomic, readonly, nonnull) NSString *resourceDirectory;

@property (nonatomic, readonly, nonnull) NSString *originImageExtension;
@property (nonatomic, readonly, nonnull) NSString *originMovieExtension;
@property (nonatomic, readonly, nonnull) NSString *previewImageExtension;
@property (nonatomic, readonly, nonnull) NSString *attachmentDivider;

@property (nonatomic, readonly, nonnull) UIFont *navigationBarFont;
@property (nonatomic, readonly, nonnull) UIColor *mainBackgroundColor;
@property (nonatomic, readonly, nonnull) UIColor *mainTintColor;
@property (nonatomic, readonly, nonnull) UIColor *mainTextColor;

@property (nonatomic, readonly, nonnull) NSString *dateFormat;

@property (nonatomic, readonly, nonnull) NSString *okTitle;
@property (nonatomic, readonly, nonnull) NSString *createNoteTitle;

@property (nonatomic, readonly) NSInteger attachmentLength;

@end
