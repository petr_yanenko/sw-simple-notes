//
// Created by Petr Yanenko on 9/11/16.
// Copyright (c) 2016 Petr Yanenko. All rights reserved.
//

#import "NSFileManager+Customizing.h"
#import "APPSErrorHandler.h"


@implementation NSFileManager (Customizing)

- (BOOL)apps_deleteItemAtPath:(NSString *)path {
    return [self apps_deleteItemAtPath:path errorDomain:@"NSFileManager+Customizing" errorCode:0];
}

- (BOOL)apps_deleteItemAtPath:(NSString *)path errorDomain:(NSString *)domain errorCode:(NSInteger)code {
    NSError *error;
    BOOL success = YES;
    if ([self fileExistsAtPath:path]) {
        if (![self removeItemAtPath:path error:&error]) {
            [ErrorHandler handleErrorInDomain:domain withCode:code description:[error localizedDescription]];
            success = NO;
        }
    }
    return success;
}

- (BOOL)apps_moveItemAtPath:(NSString *)path toPath:(NSString *)newPath errorDomain:(NSString *)domain errorCode:(NSInteger)code {
    NSError *error;
    BOOL result = [self moveItemAtPath:path toPath:newPath error:&error];
    if (!result) {
        [ErrorHandler handleErrorInDomain:domain withCode:code description:[error localizedDescription]];
    }
    return result;
}

@end