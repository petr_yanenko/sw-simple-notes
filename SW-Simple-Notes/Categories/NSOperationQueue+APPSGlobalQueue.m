//
// Created by Petr Yanenko on 8/31/15.
// Copyright (c) 2015 Applikey Solutions. All rights reserved.
//

#import "NSOperationQueue+APPSGlobalQueue.h"


@implementation NSOperationQueue (APPSGlobalQueue)

+ (NSOperationQueue *)apps_globalQueue {
    static NSOperationQueue *queue;
    static dispatch_once_t token;
    dispatch_once(&token, ^{
        queue = [[NSOperationQueue alloc] init];
        queue.maxConcurrentOperationCount = 1;
    });
    return queue;
}

+ (BOOL)apps_addOperation:(NSOperation *)operation {
    NSArray *operations = [[NSOperationQueue apps_globalQueue] operations];
    __block BOOL found = NO;
    [operations enumerateObjectsUsingBlock:^(NSOperation *obj, NSUInteger idx, BOOL *stop) {
        if (![obj isExecuting] && ![obj isFinished] && ![obj isCancelled] && [operation isEqual:obj]) {
            found = YES;
        }
    }];
    if (!found) {
        [[NSOperationQueue apps_globalQueue] addOperation:operation];
    }
    return !found;
}

@end
