//
// Created by Petr Yanenko on 8/31/15.
// Copyright (c) 2015 Applikey Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSOperationQueue (APPSGlobalQueue)

+ (NSOperationQueue *)apps_globalQueue;
+ (BOOL)apps_addOperation:(NSOperation *)operation;

@end