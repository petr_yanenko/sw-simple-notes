//
// Created by Petr Yanenko on 8/16/16.
// Copyright (c) 2016 Petr Yanenko. All rights reserved.
//

#import <CoreData/CoreData.h>
#import "NSArray+Finding.h"
#import "APPSObject.h"

@implementation NSArray (Finding)

- (id)apps_findSelected {
    __block APPSObject *selectedObject = nil;
    [self enumerateObjectsUsingBlock:^(APPSObject *obj, NSUInteger idx, BOOL *stop) {
        if (obj.selected) {
            selectedObject = obj;
            *stop = YES;
        }
    }];
    return selectedObject;
}

- (id)apps_findObject:(NSString *)objectID {
    __block APPSObject *object = nil;
    [self enumerateObjectsUsingBlock:^(APPSObject *obj, NSUInteger idx, BOOL *stop) {
        if ([obj.objectID isEqualToString:objectID]) {
            object = obj;
            *stop = YES;
        }
    }];
    return object;
}

@end