//
// Created by Petr Yanenko on 8/23/16.
// Copyright (c) 2016 Petr Yanenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIView (LayoutEngine)

- (void)apps_addEdgeConstraints:(UIEdgeInsets)insets;
- (void)apps_addEdgeConstraintsWithTopGuide:(id <UILayoutSupport>)topLayoutGuide
                                bottomGuide:(id <UILayoutSupport>)bottomLayoutGuide
                            edgeConstraints:(UIEdgeInsets)insets;
- (void)apps_addCenterConstraints:(CGPoint)center;

- (NSLayoutConstraint *)apps_addEdgeConstraintWithAttribute:(NSLayoutAttribute)attribute;

- (NSLayoutConstraint *)apps_addConstraintWithOwnAttribute:(NSLayoutAttribute)attribute
                                                      view:(UIView *)view
                                             viewAttribute:(NSLayoutAttribute)viewAttribute;

- (NSLayoutConstraint *)apps_addConstraintWithTopGuide:(id<UILayoutSupport>)topLayoutGuide;
- (NSLayoutConstraint *)apps_addConstraintWithBottomGuide:(id<UILayoutSupport>)bottomLayoutGuide;

- (NSLayoutConstraint *)apps_addConstraintWithOwnAttribute:(NSLayoutAttribute)attribute
                                                multiplier:(CGFloat)multiplier
                                                  constant:(CGFloat)constant
                                                  relation:(NSLayoutRelation)relation
                                                  topGuide:(id<UILayoutSupport>)guide
                                            guideAttribute:(NSLayoutAttribute)guideAttribute;
- (NSLayoutConstraint *)apps_addConstraintWithOwnAttribute:(NSLayoutAttribute)attribute
                                                multiplier:(CGFloat)multiplier
                                                  constant:(CGFloat)constant
                                                  relation:(NSLayoutRelation)relation
                                               bottomGuide:(id<UILayoutSupport>)guide
                                            guideAttribute:(NSLayoutAttribute)guideAttribute;

- (void)apps_addSubview:(UIView *)subview;

- (NSLayoutConstraint *)apps_addConstraintWithOwnAttribute:(NSLayoutAttribute)attribute
                                                multiplier:(CGFloat)multiplier
                                                  constant:(CGFloat)constant
                                                  relation:(NSLayoutRelation)relation
                                                      view:(UIView *)view
                                             viewAttribute:(NSLayoutAttribute)viewAttribute;


@end