//
// Created by Petr Yanenko on 8/15/16.
// Copyright (c) 2016 Petr Yanenko. All rights reserved.
//

#import "NSObject+Introspection.h"


@implementation NSObject (Introspection)

- (NSString *)apps_className {
    return NSStringFromClass([self class]);
}

+ (NSString *)apps_className {
    return NSStringFromClass(self);
}

@end