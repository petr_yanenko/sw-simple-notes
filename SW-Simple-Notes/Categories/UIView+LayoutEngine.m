//
// Created by Petr Yanenko on 8/23/16.
// Copyright (c) 2016 Petr Yanenko. All rights reserved.
//

#import "UIView+LayoutEngine.h"


@implementation UIView (LayoutEngine)

- (BOOL)checkAttributesPair:(NSLayoutAttribute [])pair {
    BOOL result = NO;
    NSInteger const attributesNumbers[] = {2, 2, 2, 2, 3, 3, 1};
    NSLayoutAttribute leadingPairEdges[] = {NSLayoutAttributeLeading, NSLayoutAttributeTrailing};
    NSLayoutAttribute leftPairEdges[] = {NSLayoutAttributeLeft, NSLayoutAttributeRight};
    NSLayoutAttribute topPairEdges[] = {NSLayoutAttributeTop, NSLayoutAttributeBottom};
    NSLayoutAttribute centers[] = {NSLayoutAttributeCenterX, NSLayoutAttributeCenterY};
    NSLayoutAttribute dimensions[] = {NSLayoutAttributeWidth, NSLayoutAttributeHeight, NSLayoutAttributeNotAnAttribute};
    NSLayoutAttribute baselines[] = {NSLayoutAttributeBaseline, NSLayoutAttributeLastBaseline, NSLayoutAttributeFirstBaseline};
    NSLayoutAttribute notAttribute[] = {NSLayoutAttributeNotAnAttribute};

    NSInteger const groupsNumber = 7;
    NSLayoutAttribute *groups[] = {
            leadingPairEdges, leftPairEdges, topPairEdges, centers, dimensions, baselines, notAttribute
    };

    NSInteger pairSize = 2;
    for (int i = 0; i < groupsNumber; ++i) {
        NSLayoutAttribute *attributes = groups[i];
        NSInteger size = attributesNumbers[i];
        NSInteger success = 0;
        for (int j = 0; j < size; ++j) {
            NSLayoutAttribute currentAttribute = attributes[j];
            for (int k = 0; k < pairSize; ++k) {
                NSLayoutAttribute pairAttribute = pair[k];
                if (pairAttribute == currentAttribute) {
                    success++;
                }
            }
        }
        if (success == pairSize) {
            result = YES;
            break;
        }
    }

    return result;
}

- (NSLayoutConstraint *)apps_addConstraintWithOwnAttribute:(NSLayoutAttribute)attribute
                                                multiplier:(CGFloat)multiplier
                                                  constant:(CGFloat)constant
                                                  relation:(NSLayoutRelation)relation
                                                      view:(UIView *)view
                                             viewAttribute:(NSLayoutAttribute)viewAttribute {
    UIView *superview, *rightView = self, *leftView = view;
    NSLayoutAttribute rightAttribute = attribute, leftAttribute = viewAttribute;
    __block NSLayoutRelation constraintRelation = relation;
    __block CGFloat constraintMultiplier = multiplier, constraintConstant = constant;

    NSString *domain = @"UIView+LayoutEngine";
    NSLayoutAttribute pair[] = {attribute, viewAttribute};
    if ( ![self checkAttributesPair:pair]) {
        NSString *reason = @"Attributes are not compatible";
        @throw [NSException exceptionWithName:domain reason:reason userInfo:nil];
    }

    void (^changeBlock) (void) = ^{
        constraintConstant = constant * -1;
        constraintMultiplier = 1 / multiplier;
        constraintRelation = relation * -1;
    };

    if ([self.superview isEqual:view]) {
        superview = view;
        if (attribute == NSLayoutAttributeTop ||
                attribute == NSLayoutAttributeLeading ||
                attribute == NSLayoutAttributeLeft) {
            changeBlock ();
        }
    } else if ([view.superview isEqual:self]) {
        superview = self;
        if (attribute == NSLayoutAttributeBottom ||
                attribute == NSLayoutAttributeTrailing ||
                attribute == NSLayoutAttributeRight) {
            changeBlock ();
        }
    } else if ([self.superview isEqual:view.superview]) {
        superview = self.superview;
        if (attribute == NSLayoutAttributeTop ||
                attribute == NSLayoutAttributeLeading ||
                attribute == NSLayoutAttributeLeft) {
            changeBlock ();
        }
    } else if (view == nil) {
        superview = leftView = self;
        leftAttribute = attribute;
        rightView = view;
        rightAttribute = viewAttribute;
    } else {
        NSString *reason = @"Views are not compatible";
        @throw [NSException exceptionWithName:domain reason:reason userInfo:nil];
    }

    NSLayoutConstraint *constraint = [NSLayoutConstraint constraintWithItem:leftView
                                                                  attribute:leftAttribute
                                                                  relatedBy:constraintRelation
                                                                     toItem:rightView
                                                                  attribute:rightAttribute
                                                                 multiplier:constraintMultiplier
                                                                   constant:constraintConstant
    ];
    [superview addConstraint:constraint];
    return constraint;
}

- (NSLayoutConstraint *)apps_addConstraintWithOwnAttribute:(NSLayoutAttribute)attribute
                                                multiplier:(CGFloat)multiplier
                                                  constant:(CGFloat)constant
                                                  relation:(NSLayoutRelation)relation
                                                  topGuide:(id<UILayoutSupport>)guide
                                            guideAttribute:(NSLayoutAttribute)guideAttribute {
    return [self apps_addConstraintWithOwnAttribute:attribute
                                         multiplier:multiplier
                                           constant:-constant
                                           relation:relation
                                              guide:guide
                                     guideAttribute:guideAttribute];
}

- (NSLayoutConstraint *)apps_addConstraintWithOwnAttribute:(NSLayoutAttribute)attribute
                                                multiplier:(CGFloat)multiplier
                                                  constant:(CGFloat)constant
                                                  relation:(NSLayoutRelation)relation
                                               bottomGuide:(id<UILayoutSupport>)guide
                                            guideAttribute:(NSLayoutAttribute)guideAttribute {
    return [self apps_addConstraintWithOwnAttribute:attribute
                                         multiplier:multiplier
                                           constant:constant
                                           relation:relation
                                              guide:guide
                                     guideAttribute:guideAttribute];
}

- (NSLayoutConstraint *)apps_addConstraintWithOwnAttribute:(NSLayoutAttribute)attribute
                                                multiplier:(CGFloat)multiplier
                                                  constant:(CGFloat)constant
                                                  relation:(NSLayoutRelation)relation
                                                     guide:(id<UILayoutSupport>)guide
                                            guideAttribute:(NSLayoutAttribute)guideAttribute {
    NSLayoutConstraint *constraint = [NSLayoutConstraint constraintWithItem:guide
                                                                  attribute:guideAttribute
                                                                  relatedBy:relation
                                                                     toItem:self
                                                                  attribute:attribute
                                                                 multiplier:multiplier
                                                                   constant:constant
    ];
    [self.superview addConstraint:constraint];
    return constraint;
}


- (void)apps_addEdgeConstraints:(UIEdgeInsets)insets {
    [self apps_addConstraintWithOwnAttribute:NSLayoutAttributeTop
                                  multiplier:1.0
                                    constant:insets.top
                                    relation:NSLayoutRelationEqual
                                        view:self.superview
                               viewAttribute:NSLayoutAttributeTop];
    [self apps_addConstraintWithOwnAttribute:NSLayoutAttributeBottom
                                  multiplier:1.0
                                    constant:insets.bottom
                                    relation:NSLayoutRelationEqual
                                        view:self.superview
                               viewAttribute:NSLayoutAttributeBottom];
    [self apps_addConstraintWithOwnAttribute:NSLayoutAttributeLeading
                                  multiplier:1.0
                                    constant:insets.left
                                    relation:NSLayoutRelationEqual
                                        view:self.superview
                               viewAttribute:NSLayoutAttributeLeading];
    [self apps_addConstraintWithOwnAttribute:NSLayoutAttributeTrailing
                                  multiplier:1.0
                                    constant:insets.right
                                    relation:NSLayoutRelationEqual
                                        view:self.superview
                               viewAttribute:NSLayoutAttributeTrailing];

}

- (void)apps_addEdgeConstraintsWithTopGuide:(id <UILayoutSupport>)topLayoutGuide
                                bottomGuide:(id <UILayoutSupport>)bottomLayoutGuide
                            edgeConstraints:(UIEdgeInsets)insets {
    [self apps_addConstraintWithOwnAttribute:NSLayoutAttributeLeading
                                  multiplier:1.0
                                    constant:insets.left
                                    relation:NSLayoutRelationEqual
                                        view:self.superview
                               viewAttribute:NSLayoutAttributeLeading];
    [self apps_addConstraintWithOwnAttribute:NSLayoutAttributeTrailing
                                  multiplier:1.0
                                    constant:insets.right
                                    relation:NSLayoutRelationEqual
                                        view:self.superview
                               viewAttribute:NSLayoutAttributeTrailing];
    [self apps_addConstraintWithOwnAttribute:NSLayoutAttributeTop
                                  multiplier:1.0
                                    constant:insets.top
                                    relation:NSLayoutRelationEqual
                                    topGuide:topLayoutGuide
                              guideAttribute:NSLayoutAttributeBottom];
    [self apps_addConstraintWithOwnAttribute:NSLayoutAttributeBottom
                                  multiplier:1.0
                                    constant:insets.bottom
                                    relation:NSLayoutRelationEqual
                                 bottomGuide:bottomLayoutGuide
                              guideAttribute:NSLayoutAttributeTop];
}

- (void)apps_addCenterConstraints:(CGPoint)center {
    [self apps_addConstraintWithOwnAttribute:NSLayoutAttributeCenterX
                                  multiplier:1.0
                                    constant:center.x
                                    relation:NSLayoutRelationEqual
                                        view:self.superview
                               viewAttribute:NSLayoutAttributeCenterX];
    [self apps_addConstraintWithOwnAttribute:NSLayoutAttributeCenterY
                                  multiplier:1.0
                                    constant:center.y
                                    relation:NSLayoutRelationEqual
                                        view:self.superview
                               viewAttribute:NSLayoutAttributeCenterY];
}

- (NSLayoutConstraint *)apps_addEdgeConstraintWithAttribute:(NSLayoutAttribute)attribute {
    return [self apps_addEdgeConstraintWithAttribute:attribute
                                          multiplier:1.0
                                            constant:0.0];
}

- (NSLayoutConstraint *)apps_addEdgeConstraintWithAttribute:(NSLayoutAttribute)attribute
                                                 multiplier:(CGFloat)multiplier
                                                   constant:(CGFloat)constant {
    return [self apps_addConstraintWithOwnAttribute:attribute
                                         multiplier:multiplier
                                           constant:constant
                                           relation:NSLayoutRelationEqual
                                               view:self.superview
                                      viewAttribute:attribute];
}


- (NSLayoutConstraint *)apps_addConstraintWithOwnAttribute:(NSLayoutAttribute)attribute
                                                      view:(UIView *)view
                                             viewAttribute:(NSLayoutAttribute)viewAttribute {
    return [self apps_addConstraintWithOwnAttribute:attribute
                                         multiplier:1.0
                                           constant:0.0
                                           relation:NSLayoutRelationEqual
                                               view:view
                                      viewAttribute:viewAttribute];
}

- (NSLayoutConstraint *)apps_addConstraintWithTopGuide:(id <UILayoutSupport>)topLayoutGuide {
    return [self apps_addConstraintWithOwnAttribute:NSLayoutAttributeTop
                                         multiplier:1.0
                                           constant:0.0
                                           relation:NSLayoutRelationEqual
                                           topGuide:topLayoutGuide
                                     guideAttribute:NSLayoutAttributeBottom];
}

- (NSLayoutConstraint *)apps_addConstraintWithBottomGuide:(id<UILayoutSupport>)bottomLayoutGuide {
    return [self apps_addConstraintWithOwnAttribute:NSLayoutAttributeBottom
                                         multiplier:1.0
                                           constant:0.0
                                           relation:NSLayoutRelationEqual
                                        bottomGuide:bottomLayoutGuide
                                     guideAttribute:NSLayoutAttributeTop];
}

- (void)apps_addSubview:(UIView *)subview {
    subview.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:subview];
}

@end