//
// Created by Petr Yanenko on 8/16/16.
// Copyright (c) 2016 Petr Yanenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray<T> (Finding)

- (T)apps_findSelected;
- (T)apps_findObject:(NSString *)objectID;

@end