//
// Created by Petr Yanenko on 8/15/16.
// Copyright (c) 2016 Petr Yanenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (Introspection)

@property (strong, nonatomic, readonly) NSString *apps_className;

+ (NSString *)apps_className;

@end