//
// Created by Petr Yanenko on 9/11/16.
// Copyright (c) 2016 Petr Yanenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSFileManager (Customizing)

- (BOOL)apps_deleteItemAtPath:(NSString *)path;
- (BOOL)apps_deleteItemAtPath:(NSString *)path errorDomain:(NSString *)domain errorCode:(NSInteger)code;

- (BOOL)apps_moveItemAtPath:(NSString *)path toPath:(NSString *)newPath errorDomain:(NSString *)domain errorCode:(NSInteger)code;

@end