//
// Created by Petr Yanenko on 8/14/16.
// Copyright (c) 2016 Petr Yanenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "APPSDBManager.h"

@interface APPSDBManager (Folder)

- (void)updateFolder:(APPSFolder *)folder;
- (void)deleteFolder:(NSString *)folderID;

@end