//
// Created by Petr Yanenko on 8/14/16.
// Copyright (c) 2016 Petr Yanenko. All rights reserved.
//

#import "APPSDBManager+Folder.h"
#import "APPSFolder.h"
#import "APPSDBFolder.h"
#import "APPSDBFolder.h"
#import "APPSErrorHandler.h"


@implementation APPSDBManager (Folder)

- (void)updateFolder:(APPSFolder *)folder {
    [self.managedObjectContext performBlockAndWait:^{
        NSError *error;
        APPSDBFolder *updatedFolder = [self.managedObjectContext existingObjectWithID:[self objectIDWithString:folder.objectID]
                                                                                error:&error];
        if (updatedFolder != nil) {
            updatedFolder.title = folder.title;
            updatedFolder.selected = folder.selected;
            [self saveIfNoTransaction];
        } else {
            [[APPSErrorHandler sharedInstance] handleErrorInDomain:@"APPSDBManager+Folder" withCode:0 description:error.localizedDescription];
        }
    }];
}

- (void)deleteFolder:(NSString *)folderID {
    [self deleteObjectWithObjectID:folderID];
}

@end
