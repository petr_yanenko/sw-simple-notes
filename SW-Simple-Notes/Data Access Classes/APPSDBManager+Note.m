//
// Created by Petr Yanenko on 8/14/16.
// Copyright (c) 2016 Petr Yanenko. All rights reserved.
//

#import "APPSDBManager+Note.h"
#import "APPSNote.h"
#import "APPSDBNote.h"
#import "APPSErrorHandler.h"
#import "APPSDBContent.h"
#import "NSObject+Introspection.h"
#import "APPSErrorHandler.h"


@implementation APPSDBManager (Note)

- (void)updateNote:(APPSNote *)note {
    [self.managedObjectContext performBlockAndWait:^{
        NSError *error;
        APPSDBNote *updatedNote = [self.managedObjectContext existingObjectWithID:[self objectIDWithString:note.objectID]
                                                                            error:&error];
        if (updatedNote != nil) {
            updatedNote.selected = note.selected;
            [self saveIfNoTransaction];
        } else {
            [[APPSErrorHandler sharedInstance] handleErrorInDomain:@"APPSDBManager+Note" withCode:1 description:error.localizedDescription];
        }
    }];
}

- (void)updateNoteContent:(APPSNote *)note {
    [self.managedObjectContext performBlockAndWait:^{
        NSError *modelObjectError;
        APPSDBNote *updatedNote = [self.managedObjectContext existingObjectWithID:[self objectIDWithString:note.objectID]
                                                                            error:&modelObjectError];
        if (updatedNote != nil) {
            NSDate *now = [NSDate date];
            updatedNote.lastEdited = [now timeIntervalSince1970];
            note.lastEdited = now;
            NSError *error;
            NSData *content = [NSJSONSerialization dataWithJSONObject:note.content options:0 error:&error];
            if (content == nil) {
                [ErrorHandler handleErrorInDomain:@"APPSDBManager+Note" withCode:0 description:error.localizedDescription];
            }
            updatedNote.content = content;
            [self saveIfNoTransaction];
        } else {
            [[APPSErrorHandler sharedInstance] handleErrorInDomain:@"APPSDBManager+Note" withCode:2 description:modelObjectError.localizedDescription];
        }
    }];
}

- (void)deleteNote:(NSString *)noteID {
    [self deleteObjectWithObjectID:noteID];
}

@end
