//
//  DBManager.m
//  SW-Simple-Notes
//
//  Created by Petr Yanenko on 8/14/16.
//  Copyright © 2016 Petr Yanenko. All rights reserved.
//

#import "APPSDBManager.h"
#import "APPSErrorHandler.h"
#import "APPSDBFolder.h"
#import "APPSDBNote.h"
#import "APPSFolder.h"
#import "APPSNote.h"
#import "NSObject+Introspection.h"
#import "APPSBlockOperation.h"

@interface APPSDBManager ()

@property (strong, readwrite) NSManagedObjectContext *managedObjectContext;
@property (assign) NSInteger transaction;

@end

@implementation APPSDBManager

- (id)init
{
    self = [super init];
    if (!self) return nil;
    
    [self initializeCoreData];
    
    return self;
}

- (void)initializeCoreData
{
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Model" withExtension:@"momd"];
    NSManagedObjectModel *mom = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    if (mom == nil) {
        [ErrorHandler handleErrorInDomain:[self apps_className] withCode:0 description:@"Error initializing Managed Object Model"];
        return;
    }
    
    NSPersistentStoreCoordinator *psc = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:mom];
    NSManagedObjectContext *moc = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [moc setPersistentStoreCoordinator:psc];
    [self setManagedObjectContext:moc];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *documentsURL = [[fileManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
    NSURL *storeURL = [documentsURL URLByAppendingPathComponent:@"Model.sqlite"];

    APPSBlockOperation *storeOperation = [[APPSBlockOperation alloc] initWithTimeDelta:0 block:^(void) {
        NSError *error = nil;
        NSPersistentStoreCoordinator *psc = [[self managedObjectContext] persistentStoreCoordinator];
        NSDictionary *options = @{
                NSMigratePersistentStoresAutomaticallyOption : @YES/*,
                NSInferMappingModelAutomaticallyOption : @YES*/
        };
        NSPersistentStore *store = [psc addPersistentStoreWithType:NSSQLiteStoreType
                                                     configuration:nil
                                                               URL:storeURL
                                                           options:options
                                                             error:&error
                                    ];
        if (store == nil) {
            [ErrorHandler handleErrorInDomain:[self apps_className] withCode:1 description:[error localizedDescription]];
        }
    }];
    [storeOperation start];
}

- (NSArray<APPSFolder *> *)allFolders {
    __block NSArray<APPSFolder *> *folders;
    [self.managedObjectContext performBlockAndWait:^{
        folders = [self fetchAllInstancesWithClass:[APPSDBFolder class]
                                   prototypeObject:[[APPSFolder alloc] initWithDBManager:self]
                                         predicate:nil
                                 sortDescriptorKey:@"title"
                                includeSubentities:NO
                                         errorCode:2];
    }];
    return folders;
}

- (NSArray<APPSNote *> *)allNotesForFolder:(NSString *)folderID {
    __block NSArray<APPSNote *> *notes;
    [self.managedObjectContext performBlockAndWait:^{
        NSError *error;
        NSManagedObject *folder = [self.managedObjectContext existingObjectWithID:[self objectIDWithString:folderID]
                                                                            error:&error];
        if (folder != nil) {
            notes = [self fetchAllInstancesWithClass:[APPSDBNote class]
                                     prototypeObject:[[APPSNote alloc] initWithDBManager:self]
                                           predicate:[NSPredicate predicateWithFormat:@"folder == %@", folder]
                                   sortDescriptorKey:@"lastEdited"
                                  includeSubentities:YES
                                           errorCode:3];
        } else {
            [ErrorHandler handleErrorInDomain:self.apps_className withCode:5 description:error.localizedDescription];
        }
    }];
    return notes;
}

- (void)createFolderWithTitle:(NSString *)title {
    [self.managedObjectContext performBlockAndWait:^{
        APPSDBFolder *newFolder = [NSEntityDescription insertNewObjectForEntityForName:[APPSDBFolder apps_className]
                                                                inManagedObjectContext:self.managedObjectContext];
        newFolder.title = title;
        [self saveIfNoTransaction];
    }];
}

- (void)addNoteToFolder:(NSString *)folderID {
    [self.managedObjectContext performBlockAndWait:^{
        NSError *error;
        APPSDBFolder *folder = [self.managedObjectContext existingObjectWithID:[self objectIDWithString:folderID]
                                                                         error:&error];
        if (folder != nil) {
            APPSDBNote *newNote = [NSEntityDescription insertNewObjectForEntityForName:[APPSDBNote apps_className]
                                                                inManagedObjectContext:self.managedObjectContext];
            newNote.lastEdited = [[NSDate date] timeIntervalSince1970];
            [folder addNotesObject:newNote];
            [self saveIfNoTransaction];
        } else {
            [ErrorHandler handleErrorInDomain:self.apps_className withCode:6 description:error.localizedDescription];
        }
    }];
}

- (BOOL)saveWithBlock:(void (^)())block {
    __block BOOL result = NO;
    self.transaction ++;
    if (block != NULL) {
        block();
    }
    [self.managedObjectContext performBlockAndWait:^{
        NSError *error;
        result = [self.managedObjectContext save:&error];
        if (!result) {
            [ErrorHandler handleErrorInDomain:[self apps_className]
                                    withCode:4
                                 description:[error localizedDescription]];
        }
    }];
    self.transaction --;
    return result;
}

- (BOOL)save {
    return [self saveWithBlock:NULL];
}

- (void)saveIfNoTransaction {
    if (self.transaction == 0) {
        [self save];
    }
}

- (void)deleteObjectWithObjectID:(NSString *)objectID {
    [self.managedObjectContext performBlockAndWait:^{
        NSError *error;
        APPSDBObject *object = [self.managedObjectContext existingObjectWithID:[self objectIDWithString:objectID]
                                                                         error:&error];
        if (object != nil) {
            [self.managedObjectContext deleteObject:object];
            [self saveIfNoTransaction];
        } else {
            [ErrorHandler handleErrorInDomain:self.apps_className withCode:7 description:error.localizedDescription];
        }
    }];
}

#pragma mark private
- (NSArray *)executeRequestWithClass:(Class)entityClass
                           predicate:(NSPredicate *)predicate
                   sortDescriptorKey:(NSString *)key
                  includeSubentities:(BOOL)subentities
                               error:(NSError * __autoreleasing *)error {
    __block NSArray *instances = nil;
    [self.managedObjectContext performBlockAndWait:^{
        NSString *entityName = [entityClass apps_className];
        NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:entityName];
        request.predicate = predicate;
        request.includesSubentities = subentities;
        if (key.length) {
            NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:key ascending:YES];
            request.sortDescriptors = @[descriptor];
        }
        instances = [self.managedObjectContext executeFetchRequest:request error:error];
    }];
    return instances;
}

- (NSArray *)fetchAllInstancesWithClass:(Class)entityClass
                        prototypeObject:(APPSObject *)prototype
                              predicate:(NSPredicate *)predicate
                      sortDescriptorKey:(NSString *)key
                     includeSubentities:(BOOL)subentities
                                  errorCode:(NSInteger)errorCode {
    NSError *requestError;
    NSArray *dbInstances = [self executeRequestWithClass:entityClass
                                               predicate:predicate
                                       sortDescriptorKey:key
                                      includeSubentities:subentities
                                                   error:&requestError];
    if (dbInstances == nil) {
        [ErrorHandler handleErrorInDomain:[self apps_className] withCode:errorCode description:[requestError localizedDescription]];
    }
    NSMutableArray *instances = [[NSMutableArray alloc] init];
    [dbInstances enumerateObjectsUsingBlock:^(APPSDBObject *obj, NSUInteger idx, BOOL *stop) {
        [instances addObject:[prototype copyWithDBObject:obj]];
    }];
    return [instances copy];
}

- (NSManagedObjectID *)objectIDWithString:(NSString *)objectID {
    return [self.managedObjectContext.persistentStoreCoordinator managedObjectIDForURIRepresentation:
            [NSURL URLWithString:objectID]
            ];
}

@end
