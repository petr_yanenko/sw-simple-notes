//
//  DBManager.h
//  SW-Simple-Notes
//
//  Created by Petr Yanenko on 8/14/16.
//  Copyright © 2016 Petr Yanenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class APPSFolder, APPSNote;
@class APPSDBFolder;

@interface APPSDBManager : NSObject

@property (strong, readonly) NSManagedObjectContext *managedObjectContext;

- (void)initializeCoreData;

- (NSArray<APPSFolder *> *)allFolders;
- (NSArray<APPSNote *> *)allNotesForFolder:(NSString *)folderID;

- (void)createFolderWithTitle:(NSString *)title;
- (void)addNoteToFolder:(NSString *)folderID;

- (BOOL)saveWithBlock:(void (^)())block;
- (BOOL)save;
- (void)saveIfNoTransaction;

- (void)deleteObjectWithObjectID:(NSString *)objectID;

- (NSManagedObjectID *)objectIDWithString:(NSString *)objectID;

@end
