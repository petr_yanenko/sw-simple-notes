//
//  BaseToolbarController.swift
//  SW-Simple-Notes
//
//  Created by Petr Yanenko on 9/23/16.
//  Copyright © 2016 Petr Yanenko. All rights reserved.
//

import UIKit

class BaseToolbarController: BaseViewController {
    
    lazy var _toolbar : UIToolbar = self._createToolbar();
    lazy var _contentView : UIView = self._createContentView();

// MARK: protected
    func _createToolbar() -> UIToolbar {
        return self.createToolbar(nil, action: nil);
    }

    func _createContentView() -> UIView {
        return UIView();
    }
    
    func _configureContentView() {
        
    }
    
    override func _createScreenContent() {
        super._createScreenContent();
        self._configureContentView();
    }
    
    override func _addSubviews() {
        self.view.apps_addSubview(_toolbar);
        self.view.apps_addSubview(_contentView);
    }
    
    override func _addConstraints() {
        _contentView.apps_addConstraint(withTopGuide: self.topLayoutGuide);
        _contentView.apps_addEdgeConstraint(with: NSLayoutAttribute.leading);
        _contentView.apps_addEdgeConstraint(with: NSLayoutAttribute.trailing);
        _toolbar.apps_addConstraint(withOwn: NSLayoutAttribute.top, view: _contentView, viewAttribute: NSLayoutAttribute.bottom);
        _toolbar.apps_addEdgeConstraint(with: NSLayoutAttribute.leading);
        _toolbar.apps_addEdgeConstraint(with: NSLayoutAttribute.trailing);
        _bottomConstraint = _toolbar.apps_addConstraint(withBottomGuide: self.bottomLayoutGuide);
    }
    
}
