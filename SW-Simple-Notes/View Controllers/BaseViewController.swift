//
// Created by Petr Yanenko on 8/24/16.
// Copyright (c) 2016 Petr Yanenko. All rights reserved.
//

import Foundation

class BaseViewController : UIViewController {
    
    var _bottomConstraint : NSLayoutConstraint = NSLayoutConstraint();

    var _viewModel : PViewModel?;
    var _router : PRouter?;

// MARK: lifecycle
    deinit {
        self._viewModel({
            viewModel in
            viewModel.cancelAction();
        });
    }

    init() {
        super.init(nibName: nil, bundle: nil);
    }

    required init?(coder: NSCoder) {
        return nil;
        super.init(coder: coder);
    }

    override func viewDidLoad() -> Void {
        super.viewDidLoad();
        self._addObservers();
        self._createScreenContent();
    }

    override func viewWillDisappear(_ animated: Bool) -> Void {
        super.viewWillDisappear(animated);
        if (self.isBeingDismissed || self.isMovingFromParentViewController) {
            self._viewModel({
                viewModel in
                viewModel.cancelAction();
            });
        }
    }

// MARK: protected
    func _viewModel(_ callback : (_: PViewModel) -> Void) -> Void {
        if let viewModel = _viewModel {
            callback(viewModel);
        }
    }

    func _addObservers() -> Void {
        
    }

    func _createScreenContent() {
        self._addSubviews();
        self._addConstraints();
    }
    
    func _addSubviews() {
        
    }
    
    func _addConstraints() {
        
    }

}
