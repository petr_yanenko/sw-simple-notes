//
// Created by Petr Yanenko on 8/30/16.
// Copyright (c) 2016 Petr Yanenko. All rights reserved.
//

import Foundation
import MobileCoreServices
import AVFoundation
import AVKit

class ContentViewController : BaseToolbarController, UITextViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var _textView : TapableAttachmentTextView = TapableAttachmentTextView();

    var _contentViewModel : ContentViewModel { get { return _viewModel as! ContentViewModel; } }

    var _textViewDispose : RACDisposable?;
    
    var _viewDidAppear = false;

// MARK: lifecycle
    deinit {
        KeyboardController.keyboardInfo.detachObserver(self);
        _textView.delegate = nil;
        if let dispose = _textViewDispose {
            dispose.dispose();
        }
    }

    init(contentModel: APPSContentModel) {
        super.init();
        _viewModel = ContentViewModel(contentModel: contentModel);
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder);
    }

    override func viewDidLoad() -> Void {
        super.viewDidLoad();
        _contentViewModel.containerControllerBounds = self.navigationController!.view.bounds;
        _contentViewModel.isPhone = self.traitCollection.userInterfaceIdiom == UIUserInterfaceIdiom.phone;
        let barButtonItem = UIBarButtonItem(
            title: APPSConstants.sharedInstance().createNoteTitle,
            style: UIBarButtonItemStyle.plain,
            target: self,
            action: #selector(_newNoteAction)
        );
        self.navigationItem.rightBarButtonItem = barButtonItem;
    }

    override func viewDidAppear(_ animated: Bool) -> Void {
        super.viewDidAppear(animated);
        _viewDidAppear = true;
        self._becomeFirstResponder();
        self._setContent();
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews();
        _contentViewModel.textViewBounds = _textView.bounds;
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        _contentViewModel.rotateAction();
    }

// MARK: protected
    override func _configureContentView() {
        super._configureContentView();
        _textView.delegate = self;
        _textViewDispose = _textView.rac_values(forKeyPath: "attachmentCharacterIndex", observer: self).skip(1).subscribeNext {
            [weak self] nextValue in
            if let strongSelf = self {
                strongSelf._contentViewModel.attachmentTapAction(strongSelf._textView.attributedText, characterIndex: strongSelf._textView.attachmentCharacterIndex);
            }
        };
        _textView.textAlignment = NSTextAlignment.left;
        _textView.attributedText = _contentViewModel.content();
        _textView.alwaysBounceVertical = true;
        _textView.keyboardDismissMode = UIScrollViewKeyboardDismissMode.interactive;
    }

    override func _createToolbar() -> UIToolbar {
        return self.createToolbar("Add", action: #selector(_addAttachmentAction));
    }
    
    override func _createContentView() -> UIView {
        return _textView;
    }

    override func _addObservers() -> Void {
        super._addObservers();
        self._handleNewDataEvents();
        self._handleAddingAttachmentEvents();
        self._handleAttachmentEvents();
        self._handleKeyboardEvents();
    }

// MARK: TextView
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        return _contentViewModel.shouldChangeContent(textView.attributedText, range: range, text: text);
    }

    func textViewDidChangeSelection(_ textView: UITextView) -> Void {
        _contentViewModel.selectedRange = textView.selectedRange;
    }

// MARK: ImagePickerController
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String: Any]) -> Void {
        self._handleFinishedPickingMediaAction(info);
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) -> Void {
        _contentViewModel.dismissImagePickerAction();
    }

}

// MARK: private
extension ContentViewController {
    
    func _becomeFirstResponder() -> Void {
        if (self._contentViewModel.enabled && self._viewDidAppear) {
            self._textView.becomeFirstResponder();
        }
    }

    func _handleNewDataEvents() -> Void {
        _contentViewModel.attachObserver(self, forProperty: #selector(getter: APPSModel.newData)) {
            [weak self] subject in
            if let strongSelf = self {
                strongSelf._setContent();
            }
        }
        _contentViewModel.attachObserver(self, forProperty: #selector(getter: ContentViewModel.enabled)) {
            [weak self] subject in
            if let strongSelf = self {
                strongSelf._textView.isUserInteractionEnabled = strongSelf._contentViewModel.enabled;
                strongSelf._becomeFirstResponder();
            }
        }
    }

    func _handleAddingAttachmentEvents() -> Void {
        _contentViewModel.attachObserver(self, forProperty: #selector(getter: ContentViewModel.showAttachmentDialog)) {
            [weak self] subject in
            if let strongSelf = self {
                if strongSelf._contentViewModel.showAttachmentDialog {
                    let alert = UIAlertController(
                            title: NSLocalizedString("Choose Attachment", comment: "Attachment sheet title"),
                            message:nil,
                            preferredStyle: UIAlertControllerStyle.actionSheet
                            );
                    strongSelf._checkInterfaceIdiom(alert);
                    strongSelf._addAttachmentButtons(alert);

                    strongSelf.present(alert, animated: true, completion: nil);
                }
            }
        }

        _contentViewModel.attachObserver(self, forProperty: #selector(getter: ContentViewModel.showImagePicker)) {
            [weak self] subject in
            if let strongSelf = self {
                if strongSelf._contentViewModel.showImagePicker {
                    let picker = strongSelf._createPickerController(strongSelf._contentViewModel.pickerType);
                    if let pickerController = picker {
                        strongSelf.present(pickerController, animated: true, completion: nil);
                    } else {
                        strongSelf._contentViewModel.cancelShowingImagePickerAction();
                    }
                } else {
                    if strongSelf.presentedViewController != nil {
                        strongSelf.dismiss(animated: true, completion: nil);
                    }
                }
            }
        }
    }

    func _handleAttachmentEvents() -> Void {
        _contentViewModel.attachObserver(self, forProperty: #selector(getter: ContentViewModel.showOriginVideo)) {
            [weak self] subject in
            if let strongSelf = self {
                let path = strongSelf._contentViewModel.attachmentPath;
                let movieViewController = AVPlayerViewController();
                let player = AVPlayer(url: URL(fileURLWithPath: path));
                movieViewController.player = player;
                strongSelf.present(movieViewController, animated: true, completion: nil);
            }
        }
        _contentViewModel.attachObserver(self, forProperty: #selector(getter: ContentViewModel.showOriginImage)) {
            [weak self] subject in
            if let strongSelf = self {
                let imageController = ImageViewController(path: strongSelf._contentViewModel.attachmentPath);
                strongSelf.present(imageController, animated: true, completion: nil);
            }
        }
    }

    func _checkInterfaceIdiom(_ controller: UIViewController) -> Void {
        if self.traitCollection.userInterfaceIdiom == UIUserInterfaceIdiom.pad {
            controller.modalPresentationStyle = UIModalPresentationStyle.popover;
            controller.popoverPresentationController!.barButtonItem = _toolbar.items!.last;
        }
    }

    func _addAttachmentButtons(_ controller: UIAlertController) -> Void {
        let libraryAction = UIAlertAction(
                title: NSLocalizedString("Photo Library", comment:"Attachment button sheet title"),
                style: UIAlertActionStyle.default) {
            alertAction in
            self._contentViewModel.photoLibraryAction();
        }
        controller.addAction(libraryAction);

        let cameraAction = UIAlertAction(
                title: NSLocalizedString("Camera", comment:"Attachment button  sheet title"),
                style: UIAlertActionStyle.default) {
            alertAction in
            self._contentViewModel.cameraAction();
        }
        controller.addAction(cameraAction);

        let cancelAction = UIAlertAction(
                title: NSLocalizedString("Cancel", comment: "Attachment button  sheet title"),
                style: UIAlertActionStyle.default) {
            alertAction in
            self._contentViewModel.cancelPickerDialogAction();
        }
        controller.addAction(cancelAction);
    }

    func _createPickerController(_ type: UIImagePickerControllerSourceType) -> UIImagePickerController? {
        if UIImagePickerController.isSourceTypeAvailable(type) {
            let picker = UIImagePickerController();
            self._checkInterfaceIdiom(picker);
            picker.sourceType = type;
            let mediaTypes = UIImagePickerController.availableMediaTypes(for: type)!;
            var cameraMediaTypes = [String(kUTTypeImage)];
            let movieType = String(kUTTypeMovie);
            if mediaTypes.contains(movieType) {
                cameraMediaTypes.append(movieType);
            }
            picker.mediaTypes = cameraMediaTypes;
            picker.delegate = self;
            return picker;
        }
        return nil;
    }

    func _setContent() -> Void {
        if self.view.window != nil {
            _textView.attributedText = _contentViewModel.content();
            let newSelectedRange = NSMakeRange(_contentViewModel.changingLocation, 0);
            _textView.selectedRange = newSelectedRange;
            if _textView.isFirstResponder {
                _textView.scrollRangeToVisible(newSelectedRange);
            }
        }
    }

    func _newNoteAction() -> Void {
        _contentViewModel.createNewNote();
    }

    func _addAttachmentAction() -> Void {
        _contentViewModel.addAttachmentAction();
    }

    func _handleFinishedPickingMediaAction(_ info: [String: Any]) -> Void {
        let mediaType = info[UIImagePickerControllerMediaType] as! String;
        if mediaType == "public.image" {
            let image = info[UIImagePickerControllerOriginalImage] as? UIImage;
            if let originImage = image {
                _contentViewModel.insertImageAttachmentAction(originImage, content: _textView.attributedText, selectedRange: _textView.selectedRange);
            }
        } else if mediaType == "public.movie" {
            let url = info[UIImagePickerControllerMediaURL] as? URL;
            if let mediaURL = url {
                _contentViewModel.insertMovieAttachmentAction(mediaURL.path, content: _textView.attributedText, selectedRange: _textView.selectedRange);
            }
        }
    }

    func _computeBottomViewWindowRect() -> CGRect {
        return _toolbar.convert(_toolbar.bounds, to: nil);
    }

    func _handleKeyboardEvents() -> Void {
        KeyboardController.keyboardInfo.attachObserver(self, forProperty: #selector(getter: KeyboardController.keyboardHeight)) {
            [weak self]subject in
            if let strong = self {
                if strong.view.window != nil {
                    let keyboardHeight = KeyboardController.keyboardInfo.keyboardHeight;
                    let duration = KeyboardController.keyboardInfo.duration;
                    let windowRect = strong._computeBottomViewWindowRect();
                    let constant = keyboardHeight - (
                        UIApplication.shared.keyWindow!.bounds.height - windowRect.maxY - strong._bottomConstraint.constant
                    );
                    strong._performKeyboardAnimation(constant, duration: duration);
                }
            }
        }
    }

    func _performKeyboardAnimation(_ constant: CGFloat, duration: Double) -> Void {
        self.view.layoutIfNeeded();
        _bottomConstraint.constant = constant;
        UIView.animate(withDuration: duration, delay: 0, options: UIViewAnimationOptions.curveLinear, animations: {
            self.view.layoutIfNeeded();
        }, completion: nil);
    }

}
