//
// Created by Petr Yanenko on 8/29/16.
// Copyright (c) 2016 Petr Yanenko. All rights reserved.
//

import Foundation

class FoldersViewController : EditableViewController {

    var _foldersViewModel: FoldersViewModel {
        get {
            return _viewModel as! FoldersViewModel; }
    }

// MARK: lifecycle
    override init() {
        super.init();
        _viewModel = FoldersViewModel(foldersModel: APPSFoldersModel(dbManager: APPSDBManager()));
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder);
    }

// MARK: public
    func createContentViewContoller() -> ContentViewController {
        return ContentViewController(contentModel: _foldersViewModel.foldersModel.notesModel.contentModel);
    }

// MARK: protected
    override func _createToolbar() -> UIToolbar {
        return self.createToolbar("New Folder", action: #selector(_newFolderAction));
    }

    override func _addObservers() -> Void {
        super._addObservers();
        self._handleShowInsertDialogEvent();
        self._handleSelectRowEvent();
    }

}

// MARK: private
extension FoldersViewController {

    func _newFolderAction() {
        self._foldersViewModel.insertFolderAction();
    }

    func _handleShowInsertDialogEvent() -> Void {
        self._foldersViewModel.attachObserver(self, forProperty: #selector(getter: FoldersViewModel.showInsertDialog), handler: {
            [weak self] subject in
            if let strongSelf = self {
                if strongSelf._foldersViewModel.showInsertDialog {
                    let alert = UIAlertController(
                            title: NSLocalizedString("New Folder", comment: "New folder action title"),
                            message: NSLocalizedString("Enter a title for this folder", comment: "New folder action description"),
                            preferredStyle: UIAlertControllerStyle.alert
                            );
                    alert.addTextField(configurationHandler: {
                        nameField in
                        nameField.placeholder = NSLocalizedString("Name", comment: "Folder name placeholder");
                        nameField.returnKeyType = UIReturnKeyType.done;
                        let cancel = UIAlertAction(
                                title: NSLocalizedString("Cancel", comment: "New folder action cancel handler"),
                                style: UIAlertActionStyle.cancel,
                                handler: {
                                    alertAction in
                                    strongSelf._foldersViewModel.cancelInsertFolderAction();
                                });
                        alert.addAction(cancel);
                        let save = UIAlertAction(
                                title: NSLocalizedString("Save", comment: "New folder action save handler"),
                                style: UIAlertActionStyle.default,
                                handler: {
                                    saveAction in
                                    strongSelf._foldersViewModel.saveInsertedFolderAction(nameField.text!);
                                });
                        alert.addAction(save);
                    });
                    strongSelf.present(alert, animated: true, completion: nil);
                }
            }
        });
    }

    func _handleSelectRowEvent() -> Void {
        self._foldersViewModel.attachObserver(self, forProperty: #selector(getter: EditableTableViewModel.selectRow), handler: {
            [weak self] subject in
            if let strongSelf = self {
                var nextController: UIViewController;
                if strongSelf.traitCollection.userInterfaceIdiom == UIUserInterfaceIdiom.phone {
                    nextController = CollapsedNotesViewController(notesModel: strongSelf._foldersViewModel.foldersModel.notesModel);
                } else {
                    nextController = NotesViewController(notesModel: strongSelf._foldersViewModel.foldersModel.notesModel);
                }
                strongSelf.navigationController!.pushViewController(nextController, animated: true);
            }
        });
    }

}
