//
// Created by Petr Yanenko on 8/30/16.
// Copyright (c) 2016 Petr Yanenko. All rights reserved.
//

import Foundation

class NotesViewController : EditableViewController {

    var _notesViewModel: NotesViewModel {
        get {
            return _viewModel as! NotesViewModel
        }
    }

// MARK: lifecycle
    init(notesModel: APPSNotesModel) {
        super.init();
        _viewModel = NotesViewModel(notesModel: notesModel);
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder);
    }

    override func viewDidLoad() -> Void {
        super.viewDidLoad();
        let newNoteItem = UIBarButtonItem(
                title: APPSConstants.sharedInstance().createNoteTitle,
                style: UIBarButtonItemStyle.plain,
                target: self,
                action: #selector(_newNoteAction)
                );
        var items: [UIBarButtonItem] = self.navigationItem.rightBarButtonItems!;
        items.append(newNoteItem);
        self.navigationItem.rightBarButtonItems = items;
    }

}

// MARK: private
extension NotesViewController {

    func _newNoteAction() -> Void {
        self._notesViewModel.createNewNote();
    }

}
