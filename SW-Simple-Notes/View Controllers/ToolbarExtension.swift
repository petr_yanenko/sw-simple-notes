//
// Created by Petr Yanenko on 8/29/16.
// Copyright (c) 2016 Petr Yanenko. All rights reserved.
//

import Foundation

extension UIViewController {

    func createToolbar(_ actionName: String?, action: Selector?) -> UIToolbar {
        let toolbar = UIToolbar();
        self.view.apps_addSubview(toolbar);
        toolbar.backgroundColor = APPSConstants.sharedInstance().mainBackgroundColor;
        toolbar.barTintColor = APPSConstants.sharedInstance().mainBackgroundColor;
        toolbar.tintColor = APPSConstants.sharedInstance().mainTintColor;
        toolbar.isTranslucent = false;
        if let name = actionName {
            let actionButton = UIBarButtonItem(
                title: NSLocalizedString(name, comment: "View controller toolbar action button name"),
                style: UIBarButtonItemStyle.plain,
                target: self,
                action: action!
            );
            let space = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil);
            toolbar.setItems([space, actionButton], animated: false);
        }
        return toolbar;
    }

}
