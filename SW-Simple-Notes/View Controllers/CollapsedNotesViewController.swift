//
// Created by Petr Yanenko on 8/30/16.
// Copyright (c) 2016 Petr Yanenko. All rights reserved.
//

import Foundation

class CollapsedNotesViewController : NotesViewController, UINavigationControllerDelegate {

    var _detailController : Bool = false;

// MARK: lifecycle
    deinit {
        self.navigationController?.delegate = nil;
    }
    
    override func viewDidLoad() {
        super.viewDidLoad();
        self.navigationController!.delegate = self;
    }

// MARK: protected
    override func _addObservers() -> Void {
        super._addObservers();
        self._notesViewModel.attachObserver(self, forProperty: #selector(getter: EditableTableViewModel.selectRow), handler: {
            [weak self] subject in
            if let strongSelf = self {
                if strongSelf.navigationController!.visibleViewController == strongSelf {
                    strongSelf.navigationController!.pushViewController(
                        ContentViewController(contentModel: strongSelf._notesViewModel.notesModel.contentModel), animated: true
                    );
                }
            }
        });
    }

// MARK: NavigationController
    func navigationController(_ navigationController: UINavigationController, willShow controller: UIViewController, animated: Bool) {
        _detailController = controller is ContentViewController;
    }

}
