//
// Created by Petr Yanenko on 9/18/16.
// Copyright (c) 2016 Petr Yanenko. All rights reserved.
//

import Foundation

class ImageViewController : UIViewController, UIScrollViewDelegate {

    var _path : String;
    var _image : UIImage?;

    var _scrollView : UIScrollView = UIScrollView();
    var _imageView : UIImageView = UIImageView();
    lazy var _toolbar : UIToolbar = self.createToolbar("Done", action: #selector(_doneAction));

// MARK: lifecycle
    init(path: String) {
        _path = path;
        super.init(nibName: nil, bundle: nil);
    }

    required init?(coder: NSCoder) {
        return nil;
        super.init(coder: coder);
    }

    override func viewDidLoad() -> Void {
        super.viewDidLoad();
        DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async {
            let data = try? Data(contentsOf: URL(fileURLWithPath: self._path));
            DispatchQueue.main.async {
                if let unwrappedData = data {
                    self._image = UIImage(data: unwrappedData);
                }
                self._configureImageView();
            }
        }

        self.view.backgroundColor = UIColor.black;
        _toolbar = self.createToolbar("Done", action: #selector(_doneAction));
        _scrollView.delegate = self;
        self.view.apps_addSubview(_scrollView);
        _scrollView.apps_addEdgeConstraint(with: NSLayoutAttribute.top);
        _scrollView.apps_addEdgeConstraint(with: NSLayoutAttribute.leading);
        _scrollView.apps_addEdgeConstraint(with: NSLayoutAttribute.trailing);
        _toolbar.apps_addEdgeConstraint(with: NSLayoutAttribute.bottom);
        _toolbar.apps_addEdgeConstraint(with: NSLayoutAttribute.leading);
        _toolbar.apps_addEdgeConstraint(with: NSLayoutAttribute.trailing);
        _scrollView.apps_addConstraint(withOwn: NSLayoutAttribute.bottom, view: _toolbar, viewAttribute: NSLayoutAttribute.top);
    }

    override func viewWillTransition(to size: CGSize, with coordiantor: UIViewControllerTransitionCoordinator) -> Void {
        self._configureZoom();
    }

    override func viewDidLayoutSubviews() -> Void {
        super.viewDidLayoutSubviews();
        self._configureZoom();
    }

// MARK: ScrollView
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return _imageView;
    }

    func scrollViewDidZoom(_ scrollView: UIScrollView) -> Void {
        let contentSize = _scrollView.contentSize;
        let bounds = _scrollView.bounds;
        var inset = UIEdgeInsets.zero;
        if bounds.width > contentSize.width {
            inset.left = (bounds.width - contentSize.width) / 2;
        } else {
            inset.top = (bounds.height - contentSize.height) / 2;
        }

        _scrollView.contentInset = inset;
    }

}

// MARK: private
extension ImageViewController {

    func _configureImageView() -> Void {
        if self.presentingViewController != nil {
            _imageView = UIImageView(image: _image);
            _scrollView.apps_addSubview(_imageView);
            _imageView.apps_addEdgeConstraints(UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0));

            self._configureZoom();
        }
    }

    func _configureZoom() -> Void {
        if let image = _image {
            var minZoom = min(_scrollView.bounds.size.width / image.size.width, _scrollView.bounds.size.height / image.size.height);
            minZoom = min(minZoom, 1);
            _scrollView.minimumZoomScale = minZoom;
            _scrollView.maximumZoomScale = 2.0;
            _scrollView.zoomScale = _scrollView.minimumZoomScale;
        }
    }

    func _doneAction() -> Void {
        self.dismiss(animated: true, completion: nil);
    }

}
