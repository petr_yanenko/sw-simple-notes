//
// Created by Petr Yanenko on 8/24/16.
// Copyright (c) 2016 Petr Yanenko. All rights reserved.
//

import Foundation

class CustomTableViewController : BaseToolbarController, UITableViewDataSource, UITableViewDelegate {

    var _tableView : UITableView = UITableView();
    var _tableViewModel : PTableViewModel { get { return _viewModel as! PTableViewModel; } }

// MARK: lifecycle
    deinit {
        self._tableView.dataSource = nil;
        self._tableView.delegate = nil;
    }

    override func viewWillAppear(_ animated: Bool) -> Void {
        super.viewWillAppear(animated);
//        _tableView.selectRowAtIndexPath(nil, animated: true, scrollPosition: UITableViewScrollPosition.None);
    }

    override func viewDidAppear(_ animated: Bool) -> Void {
        super.viewDidAppear(animated);
        _tableView.flashScrollIndicators();
    }

    override func viewDidLayoutSubviews() -> Void {
        super.viewDidLayoutSubviews();
        _tableView.reloadData();
    }

// MARK: protected
    override func _addObservers() -> Void {
        super._addObservers();
        self._viewModel({
            viewModel in
            viewModel.attachObserver(self, forProperty: #selector(getter: APPSModel.newData), handler: {
                [weak self] subject in
                if let strongSelf = self {
                    strongSelf._tableView.reloadData();
                }
            });
        });
    }
    
    override func _createContentView() -> UIView {
        return _tableView;
    }

    override func _configureContentView() -> Void {
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.tableFooterView = UIView();
    }

    func _configureCell(_ cell: UITableViewCell, indexPath: IndexPath) -> Void {

    }

    func _createCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell();
    }

// MARK: TableView
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return _tableViewModel.rowHeight(tableView.bounds.size.height, indexPath: indexPath);
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) -> Void {
        _tableViewModel.selectRowAction(indexPath);
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return _tableViewModel.cellsNumber(section);
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: _tableViewModel.cellReuseIdentifier(indexPath));
        let newCell : UITableViewCell;
        if let unwrappedCell = cell {
            newCell = unwrappedCell;
        } else {
            newCell = self._createCell(tableView, indexPath: indexPath);
        }
        self._configureCell(newCell, indexPath: indexPath);
        return newCell;
    }

}
