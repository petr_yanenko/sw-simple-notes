//
// Created by Petr Yanenko on 8/25/16.
// Copyright (c) 2016 Petr Yanenko. All rights reserved.
//

import Foundation

class EditableViewController : CustomStyleViewController {

    var _editableViewModel : PEditableTableViewModel { get { return _viewModel as! PEditableTableViewModel; } }
    var _iOSViewModel : PiOSViewModel { get { return _viewModel as! PiOSViewModel; } }

// MARK: lifecycle
    override func viewDidLoad() -> Void {
        super.viewDidLoad();
        self.navigationItem.title = _iOSViewModel.navigationTitle;
        self.navigationItem.rightBarButtonItem = self.editButtonItem;
    }

    override func setEditing(_ editing: Bool, animated: Bool) -> Void {
        super.setEditing(editing, animated: animated);
        _tableView.setEditing(editing, animated: animated);
        if editing {
            _editableViewModel.editingDidStart();
        } else {
            _editableViewModel.editingDidEnd();
        }
    }

// MARK: protected
    override func _addObservers() -> Void {
        super._addObservers();
        _editableViewModel.attachObserver(self, forProperty: #selector(getter: EditableTableViewModel.deleteRow), handler: {
            [weak self] subject in
            if let strongSelf = self {
                strongSelf._tableView.deleteRows(
                        at: [strongSelf._editableViewModel.deletedPath],
                        with: UITableViewRowAnimation.automatic
                        );
            }
        });
        let selectHandler = {
            (strongSelf: EditableViewController) -> Void in
            strongSelf._tableView.selectRow(
                    at: strongSelf._editableViewModel.selectedPath,
                    animated:true,
                    scrollPosition: UITableViewScrollPosition.none
                    );
        }
        _editableViewModel.attachObserver(self, forProperty: #selector(getter: EditableTableViewModel.selectRow), handler: {
            [weak self] subject in
            if let strongSelf = self {
                selectHandler(strongSelf);
            }
        });
        _editableViewModel.attachObserver(self, forProperty: #selector(getter: EditableTableViewModel.highlightSelectedRow), handler: {
            [weak self] subject in
            if let strongSelf = self {
                selectHandler(strongSelf);
            }
        });
        _editableViewModel.attachObserver(self, forProperty: #selector(getter: EditableTableViewModel.moveRow), handler: {
            [weak self] subject in
            if let strongSelf = self {
                strongSelf._tableView.moveRow(
                        at: strongSelf._editableViewModel.fromIndexPath,
                        to: strongSelf._editableViewModel.toIndexPath
                        );
            }
        });
        _editableViewModel.attachObserver(self, forProperty: #selector(getter: EditableTableViewModel.reloadRow), handler: {
            [weak self] subject in
            if let strongSelf = self {
                strongSelf._tableView.reloadRows(
                        at: [strongSelf._editableViewModel.reloadIndexPath],
                        with: UITableViewRowAnimation.automatic
                        );
            }
        });
    }

    override func _createCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell (style: UITableViewCellStyle.subtitle, reuseIdentifier: _editableViewModel.cellReuseIdentifier(nil));
    }

    override func _configureCell(_ cell: UITableViewCell, indexPath: IndexPath) {
        super._configureCell(cell, indexPath: indexPath);
        let cellModel = _editableViewModel.subtitleCellViewModel(indexPath);
        cell.textLabel!.text = cellModel.title;
        cell.detailTextLabel!.text = cellModel.subtitle;
        if cellModel.selected {
            _tableView.selectRow(
                    at: indexPath,
                    animated:true,
                    scrollPosition: UITableViewScrollPosition.none
                    );
        }
    }

// MARK: TableView
    func tableView(_ tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: IndexPath) {
        _editableViewModel.deleteRowAction(indexPath);
    }

    func tableView(_ tableView: UITableView, didEndEditingRowAtIndexPath indexPath: IndexPath) {
        _editableViewModel.editingDidEnd();
    }

}
