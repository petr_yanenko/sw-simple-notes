//
// Created by Petr Yanenko on 8/24/16.
// Copyright (c) 2016 Petr Yanenko. All rights reserved.
//

import Foundation

class BaseRouter : NSObject, PRouter {

    var _controller : BaseViewController;

    init(controller: BaseViewController) {
        _controller = controller;
        super.init();
    }

}
