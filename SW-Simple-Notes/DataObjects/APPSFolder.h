//
//  APPSFolder.h
//  SW-Simple-Notes
//
//  Created by Petr Yanenko on 8/14/16.
//  Copyright © 2016 Petr Yanenko. All rights reserved.
//

#import "APPSObject.h"

@class APPSNote, APPSDBFolder;

@interface APPSFolder : APPSObject

@property (strong, nonatomic) NSString *title;
@property (assign, nonatomic, readonly) NSUInteger count;
@property (strong, nonatomic) NSArray<APPSNote *> *notes;
@property (strong, nonatomic, readonly) APPSNote *selectedNote;

@end
