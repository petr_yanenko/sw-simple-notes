//
//  DataObject.h
//  SW-Simple-Notes
//
//  Created by Petr Yanenko on 8/14/16.
//  Copyright © 2016 Petr Yanenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@class APPSDBObject;
@class APPSDBManager;

@interface APPSObject : NSObject

@property (strong, nonatomic, readonly) NSString *objectID;
@property (strong, nonatomic, readonly) NSString *percentEncodedObjectID;
@property (assign, nonatomic) BOOL selected;
@property (weak, nonatomic, readonly) APPSDBManager *dbManager;

- (instancetype)initWithDBManager:(APPSDBManager *)manager;

- (id)copyWithDBObject:(id)object;
- (void)updateObject;
- (void)deleteObject;

@end

@interface APPSObject (Protected)

- (void)copyDataFromDBObject:(id)object;

@end
