//
//  APPSNote.h
//  SW-Simple-Notes
//
//  Created by Petr Yanenko on 8/14/16.
//  Copyright © 2016 Petr Yanenko. All rights reserved.
//

#import "APPSObject.h"

static NSString *const kVideoCode = @"vid";
static NSString *const kImageCode = @"img";
static NSString *const kTextCode = @"txt";

static NSInteger const kTypeLength = 5;//txt//
static NSInteger const kTypeCodeLength = 3;//vid, img, txt

@interface APPSNote : APPSObject

@property (strong, nonatomic) NSDate *lastEdited;
@property (strong, nonatomic) NSArray<NSString *> *content;

- (void)updateContent;

@end
