//
//  APPSFolder.m
//  SW-Simple-Notes
//
//  Created by Petr Yanenko on 8/14/16.
//  Copyright © 2016 Petr Yanenko. All rights reserved.
//

#import "APPSFolder.h"
#import "APPSDBFolder.h"
#import "APPSDBManager+Folder.h"
#import "NSArray+Finding.h"

@implementation APPSFolder

- (NSUInteger)count {
    return self.notes.count;
}

- (void)updateObject {
    [self.dbManager updateFolder:self];
}

- (void)deleteObject {
    [self.dbManager deleteFolder:self.objectID];
}

- (NSArray<APPSNote *> *)notes {
    if (_notes == nil) {
       _notes = [self.dbManager allNotesForFolder:self.objectID];
    }
    return _notes;
}

- (APPSNote *)selectedNote {
    return [self.notes apps_findSelected];
}

#pragma mark protected
- (void)copyDataFromDBObject:(APPSDBFolder *)object {
    [super copyDataFromDBObject:object];
    _title = object.title;
}

@end
