//
//  APPSNote.m
//  SW-Simple-Notes
//
//  Created by Petr Yanenko on 8/14/16.
//  Copyright © 2016 Petr Yanenko. All rights reserved.
//

#import "APPSNote.h"
#import "APPSDBNote.h"
#import "APPSDBContent.h"
#import "APPSDBManager+Note.h"
#import "APPSErrorHandler.h"
#import "NSObject+Introspection.h"

@implementation APPSNote

- (void)updateObject {
    [self.dbManager updateNote:self];
}

- (void)deleteObject {
    [self.dbManager deleteNote:self.objectID];
}

- (void)updateContent {
    [self.dbManager updateNoteContent:self];
}

#pragma mark protected
- (void)copyDataFromDBObject:(APPSDBNote *)object {
    [super copyDataFromDBObject:object];
    _lastEdited = [NSDate dateWithTimeIntervalSince1970:object.lastEdited];
    NSArray<NSString *> *content = [NSArray array];
    if (object.content != nil) {
        NSError *error;
        NSArray<NSString *> *dbContent = [NSJSONSerialization JSONObjectWithData:object.content options:0 error:&error];
        if (dbContent == nil) {
            [ErrorHandler handleErrorInDomain:self.apps_className withCode:0 description:error.localizedDescription];
        } else {
            content = dbContent;
        }
    }
    _content = content;
}

@end
