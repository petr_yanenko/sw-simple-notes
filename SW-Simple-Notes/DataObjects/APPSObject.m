//
//  DataObject.m
//  SW-Simple-Notes
//
//  Created by Petr Yanenko on 8/14/16.
//  Copyright © 2016 Petr Yanenko. All rights reserved.
//

#import <CoreData/CoreData.h>
#import "APPSObject.h"
#import "APPSDBObject.h"
#import "APPSDBManager.h"

@implementation APPSObject

- (instancetype)initWithDBManager:(APPSDBManager *)manager {
    self = [super init];
    if (self) {
        _dbManager = manager;
    }
    return self;
}

- (id)copyWithDBObject:(id)object {
    APPSObject *copy = [[[self class] alloc] initWithDBManager:self.dbManager];
    [copy copyDataFromDBObject:object];
    return copy;
}

- (NSString *)percentEncodedObjectID {
    return [_objectID stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet alphanumericCharacterSet]];
}

- (void)updateObject {
    
}

- (void)deleteObject {
    
}

#pragma mark protected
- (void)copyDataFromDBObject:(APPSDBObject *)object {
    _objectID = object.objectID.URIRepresentation.absoluteString;
    _selected = object.selected;
}

@end
