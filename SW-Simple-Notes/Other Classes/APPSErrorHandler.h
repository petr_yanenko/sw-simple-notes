//
//  APPSErrorHandler.h
//  Simple Notes
//
//  Created by Petr Yanenko on 8/9/16.
//  Copyright © 2016 Petr Yanenko. All rights reserved.
//

#import <Foundation/Foundation.h>

#define ErrorHandler [APPSErrorHandler sharedInstance]

@interface APPSErrorHandler : NSObject

+ (instancetype)sharedInstance;
- (void)handleErrorInDomain:(NSString *)domain withCode:(NSInteger)code description:(NSString *)description;
- (void)handleException:(NSException *)exception;

@end
