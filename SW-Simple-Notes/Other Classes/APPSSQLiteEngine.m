//
//  APPSSQLIteManager.m
//  Simple Notes
//
//  Created by Petr Yanenko on 8/9/16.
//  Copyright © 2016 Petr Yanenko. All rights reserved.
//

#import "APPSSQLiteEngine.h"
#import <sqlite3.h>
#import "APPSErrorHandler.h"
#import "NSObject+Introspection.h"

@interface APPSSQLiteEngine ()

@property (strong, nonatomic) NSMutableDictionary<NSString *, NSValue *> *preparedStatements;

@end

@implementation APPSSQLiteEngine {
    sqlite3 *_connection;
    NSMutableArray<NSMutableArray<NSString *> *> *_data;
    NSMutableArray<NSString *> *_columnNames;
}

- (void)dealloc {
    [self checkConnection:^{
        for (NSString *key in _preparedStatements) {
            [self finalizeStatementForKey:key];
        }
        if ( ![self evaluateProcedure:^{ return sqlite3_close (_connection);}]) {
            [ErrorHandler handleErrorInDomain:[self apps_className]
                                    withCode:1
                                 description:@"There is possible leak as connection was not closed"
             ];
        }
    } failure:NULL];
}

- (instancetype)init {
    self = [super init];
    if (self) {
        NSArray<NSString *> *documentsPathes = NSSearchPathForDirectoriesInDomains(
                                                                                   NSDocumentDirectory,
                                                                                   NSUserDomainMask,
                                                                                   YES
                                                                                   );
        if (documentsPathes.count > 0) {
            [self openDB:[documentsPathes firstObject]];
        } else {
            [ErrorHandler handleErrorInDomain:[self apps_className]
                                    withCode:0
                                 description:@"Documents path is not found"
             ];
        }
    }
    return self;
}

#pragma mark Public

- (NSArray *)data {
    return [_data mutableCopy];
}

- (NSArray *)columnNames {
    return [_columnNames mutableCopy];
}

+ (instancetype)sharedInstance {
    static APPSSQLiteEngine *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[APPSSQLiteEngine alloc] init];
    });
    return instance;
}

- (void)addPreparedStatement:(NSString *)statement forKey:(NSString *)key {
    [self checkConnection:^{
        __block sqlite3_stmt *preparedStatement = NULL;
        if ( ![self evaluateProcedure:^{
            const char *cStatement = [statement UTF8String];
                return sqlite3_prepare_v2(_connection, cStatement, (int)strlen(cStatement), &preparedStatement, NULL); }
             ]
            ) {
            [ErrorHandler handleErrorInDomain:[self apps_className]
                                    withCode:3
                                 description:@"Prepared statement is not created"
             ];
        } else {
            NSValue *wrapper = [NSValue valueWithPointer:preparedStatement];
            self.preparedStatements [key] = wrapper;
        }
    } failure:NULL];
}

- (void)removePreparedStatementForKey:(NSString *)key {
    [self finalizeStatementForKey:key];
}

- (void)executeStatements:(NSString *)statements {
    [self checkConnection:^{
        char const *cStatements = [statements UTF8String];
        if ( ![self evaluateProcedure:^{
            [_data removeAllObjects];
            [_columnNames removeAllObjects];
            return sqlite3_exec(_connection, cStatements, execCallback, NULL, NULL); }
             ]
            ) {
            [ErrorHandler handleErrorInDomain:[self apps_className]
                                    withCode:5
                                 description:@"Error at exec function"
             ];
        }
    } failure:NULL];
}

- (void)executePreparedStatementWithKey:(NSString *)key values:(NSArray<NSString *> *)values {
    [self checkConnection:^{
        NSValue *wrapper = self.preparedStatements [key];
        if (wrapper == nil) {
            [ErrorHandler handleErrorInDomain:[self apps_className]
                                    withCode:6
                                 description:[NSString stringWithFormat:@"Prepared statement is missing for %@ key", key]];
            return ;
        }
        sqlite3_stmt *statement = [wrapper pointerValue];
        if ([self bindStatement:statement values:values]) {
            [self executeStatement:statement];
        }
        if ( ![self evaluateProcedure:^{ return sqlite3_reset(statement); }]) {
            [ErrorHandler handleErrorInDomain:[self apps_className]
                                    withCode:7
                                 description:[NSString stringWithFormat:@"Prepared statement isn't reseted for %@ key", key ]];
        }
    } failure:NULL];
}

#pragma mark Private

- (BOOL)bindStatement:(sqlite3_stmt *)statement values:(NSArray<NSString *> *)values {
    BOOL fail = NO;
    for (int i = 1; i <= values.count; i++) {
        const char *value = [values [i] UTF8String];
        if ( ![self evaluateProcedure:^{ return sqlite3_bind_text(statement, i, value, (int)strlen (value), NULL); }]) {
            fail = YES;
        }
        if (fail) {
            break;
        }
    }
    return !fail;
}

- (void)executeStatement:(sqlite3_stmt *)statement {
    [_data removeAllObjects];
    [_columnNames removeAllObjects];
    while (sqlite3_step(statement) == SQLITE_ROW) {
        int count = sqlite3_column_count(statement);
        NSMutableArray *rowArray = [[NSMutableArray alloc] initWithCapacity:count];
        [_data addObject:rowArray];
        NSInteger namesCount = _columnNames.count;
        for (int i = 0; i < count; i++) {
            const char *cText = (const char *)sqlite3_column_text(statement, i);
            const char *nullText = "\0";
            NSString *text = [[NSString alloc] initWithUTF8String:cText == NULL ? nullText : cText];
            [rowArray addObject:text];
            if (namesCount < count) {
                const char *cName = sqlite3_column_name(statement, i);
                NSString *name = [[NSString alloc] initWithUTF8String:cName == NULL ? nullText : cName];
                [_columnNames addObject:name];
            }
        }
    }
}

- (BOOL)evaluateProcedure:(int (^)(void))block {
    if (block () == SQLITE_OK) {
        return YES;
    }
    return NO;
}

- (void)checkConnection:(void (^)())success failure:(void (^)())failure {
    BOOL result = _connection != NULL;
    if (result) {
        if (success != NULL) {
            success ();
        }
    } else {
        if (failure != NULL) {
            failure ();
        }
    }
}

- (void)openDB:(NSString *)path {
    if ( ![self evaluateProcedure:^{ return sqlite3_open([[path stringByAppendingPathComponent:@"notes.db"] UTF8String], &_connection); }]) {
        [ErrorHandler handleErrorInDomain:[self apps_className]
                                withCode:4
                             description:@"DB is not opened"
         ];
    }
}

- (void)finalizeStatementForKey:(NSString *)key {
    NSValue *statement = self.preparedStatements [key];
    if ( ![self evaluateProcedure:^{ return sqlite3_finalize([statement pointerValue]); }]) {
        [ErrorHandler handleErrorInDomain:[self apps_className]
                                withCode:2
                             description:@"There is possible leak as prepared statement was not closed"
         ];
    } else {
        [self.preparedStatements removeObjectForKey:key];
    }
}

int execCallback (void *customData, int count, char **columns, char **columnNames) {
    NSMutableArray *names = [[NSMutableArray alloc] initWithCapacity:count];
    NSMutableArray *data = [[NSMutableArray alloc] initWithCapacity:count];
    for (int i = 0; i < count; i++) {
        if (names.count < count) {
            [names addObject:[[NSString alloc] initWithCString:columnNames [i] encoding:NSUTF8StringEncoding]];
        }
        [data addObject:[[NSString alloc] initWithCString:columns [i] encoding:NSUTF8StringEncoding]];
    }
    [APPSSQLiteEngine sharedInstance]->_data = data;
    [APPSSQLiteEngine sharedInstance]->_columnNames = names;
    
    return 0;
}

@end
