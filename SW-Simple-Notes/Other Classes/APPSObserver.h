//
//  APPSObserver.h
//  Wazere
//
//  Created by Petr Yanenko on 4/6/15.
//  Copyright (c) 2015 iOS Developer. All rights reserved.
//

#import <Foundation/Foundation.h>

@class APPSBaseSubject;
@class RACDisposable;

typedef void (^ObserverHandler)(id subject);

@interface APPSObserver : NSObject

@property (weak, nonatomic) id observer;
@property (copy, nonatomic) ObserverHandler handler;
@property (strong, nonatomic) RACDisposable *disposable;

- (instancetype)initWithObserver:(id)observer handler:(ObserverHandler)handler;

@end
