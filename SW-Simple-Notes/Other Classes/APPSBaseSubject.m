//
//  APPSBaseSubject.m
//  Wazere
//
//  Created by Petr Yanenko on 4/6/15.
//  Copyright (c) 2015 iOS Developer. All rights reserved.
//

#import "APPSBaseSubject.h"

@interface APPSBaseSubject ()

@property (strong, nonatomic) NSDictionary *observers;
@property (strong, nonatomic) NSDictionary *signals;

@end

@implementation APPSBaseSubject

- (instancetype)init {
    self = [super init];
    if (self) {
        _observers = @{};
        _signals = @{};
    }
    return self;
}

- (NSString *)keyPathForProperty:(SEL)property {
    if (property != NULL) {
        NSString *keyPath = NSStringFromSelector(property);
        return keyPath;
    }
    return NULL;
}

- (void)addSignal:(RACSignal *)signal forProperty:(SEL)property {
    NSMutableDictionary *signals = [self.signals mutableCopy];
    signals[[self keyPathForProperty:property]] = signal;
    self.signals = [signals copy];
}

- (void)addStateSignalForProperty:(SEL)property skipFirst:(BOOL)skip {
    [self addSignal:[[self propertySignal:property skipFirst:skip] distinctUntilChanged] forProperty:property];
}

- (void)addStateSignalForProperty:(SEL)property {
    [self addStateSignalForProperty:property skipFirst:YES];
}

- (void)addStreamSignalForProperty:(SEL)property {
    [self addSignal:[self propertySignal:property skipFirst:YES] forProperty:property];
}

- (RACSignal *)propertySignal:(SEL)property skipFirst:(BOOL)skip {
    RACSignal *signal = [self propertySignal:property];
    if (skip) {
        signal = [signal skip:1];
    }
    return signal;
}

- (RACSignal *)propertySignal:(SEL)property {
    return [self rac_valuesForKeyPath:[self keyPathForProperty:property] observer:self];
}

- (void (^)(id x))subscribeBlockWithObserverWrapper:(APPSObserver *)observerWrapper {
    @weakify(self);
    return ^(id _) {
        @strongify(self);
        if (observerWrapper.handler) {
            observerWrapper.handler(self);
        }
    };
}

- (void)attachObserver:(id)observer forProperty:(SEL)property handler:(ObserverHandler)handler {
    NSString *event = [self keyPathForProperty:property];
    APPSObserver *observerWrapper = [[APPSObserver alloc] initWithObserver:observer handler:handler];
    NSMutableDictionary *mutableObservers = [self.observers mutableCopy];
    NSMutableArray *eventObservers = [[self observersForEvent:event] mutableCopy];
    if (eventObservers == nil) {
        eventObservers = [@[observerWrapper] mutableCopy];
    } else {
        [eventObservers addObject:observerWrapper];
    }
    mutableObservers[event] = [eventObservers copy];
    self.observers = [mutableObservers copy];

    RACSignal *signal = self.signals[event];
    RACDisposable *disposable = [signal subscribeNext:[self subscribeBlockWithObserverWrapper:observerWrapper]];
    observerWrapper.disposable = disposable;
}

- (void)detachObserver:(id)observer {
    [self detachObserver:observer forProperty:NULL];
}

- (void)detachObserver:(id)observer forProperty:(SEL)property {
    NSString *event = [self keyPathForProperty:property];
    NSArray *keys;
    if (event.length) {
        keys = @[event];
    } else {
        keys = [self.observers allKeys];
    }
    NSMutableDictionary *mutableObservers = [self.observers mutableCopy];
    for (NSString *key in keys) {
        NSMutableArray *eventObservers = [[self observersForEvent:key] mutableCopy];
        for (NSInteger i = 0; i < eventObservers.count; i++) {
            APPSObserver *eventObserver = (APPSObserver *) eventObservers[i];
            if (eventObserver.observer == nil || [eventObserver.observer isEqual:observer]) {
                [eventObserver.disposable dispose];
                [eventObservers removeObjectAtIndex:i];
                i--;
            }
        }
        mutableObservers[key] = [eventObservers copy];
    }
    self.observers = [mutableObservers copy];
}

- (NSArray *)observersForEvent:(NSString *)event {
    return (NSArray *) self.observers[event];
}

@end
