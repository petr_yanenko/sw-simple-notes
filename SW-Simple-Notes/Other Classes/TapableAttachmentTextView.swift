//
//  TapableAttachmentTextView.swift
//  SW-Simple-Notes
//
//  Created by Petr Yanenko on 9/9/16.
//  Copyright © 2016 Petr Yanenko. All rights reserved.
//

import UIKit

class TapableAttachmentTextView: UITextView {

    let kInvalidIndex = -1;
    var _attachmentCharacterIndex = -1;
    dynamic var attachmentCharacterIndex : Int {
        get { return _attachmentCharacterIndex; }
        set { _attachmentCharacterIndex = newValue; }
    };
    
    override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if (self.selectedRange.length == 0) {
            if let tapRecognizer = gestureRecognizer as? UITapGestureRecognizer  {
                if tapRecognizer.numberOfTapsRequired == 1 && tapRecognizer.numberOfTouchesRequired == 1 {
                    return !self.isTappedMediaAttachment(gestureRecognizer.location(in: self));
                }
            }
        }
        return super.gestureRecognizerShouldBegin(gestureRecognizer);
    }
    
    func isTappedMediaAttachment(_ point: CGPoint) -> Bool {
        if self.attributedText.length == 0 {
            return false;
        }
        
        let textContainer = self.textContainer;
        let layoutManager = self.layoutManager;
        
        let shiftPoint = CGPoint(x: point.x - self.textContainerInset.left, y: point.y - self.textContainerInset.top)
        
        
        var partialFraction : CGFloat = 0;
        let characterIndex = layoutManager.characterIndex(for: shiftPoint, in: textContainer, fractionOfDistanceBetweenInsertionPoints: &partialFraction);
        if characterIndex >= self.attributedText.length {
            return false;
        }
        var range : NSRange = NSRange(location: 0, length: 0);
        let attachment = self.attributedText.attribute(NSAttachmentAttributeName, at: characterIndex, effectiveRange: &range);
        if attachment != nil {
            self.attachmentCharacterIndex = characterIndex;
            _attachmentCharacterIndex = kInvalidIndex;
        }
        
        return true;
    }

}
