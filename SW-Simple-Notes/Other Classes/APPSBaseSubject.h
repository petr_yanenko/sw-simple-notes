//
//  APPSBaseSubject.h
//  Wazere
//
//  Created by Petr Yanenko on 4/6/15.
//  Copyright (c) 2015 iOS Developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ReactiveCocoa.h>
#import <RACEXTScope.h>
#import "APPSObserver.h"

@class RACSignal;

@protocol APPSObserverProtocol <NSObject>

- (void)attachObserver:(id)observer forProperty:(SEL)property handler:(ObserverHandler)handler;

- (void)detachObserver:(id)observer forProperty:(SEL)property;

- (void)detachObserver:(id)observer;

@end

@interface APPSBaseSubject : NSObject <APPSObserverProtocol>

@end

@interface APPSBaseSubject (Protected)

- (void)addStateSignalForProperty:(SEL)property skipFirst:(BOOL)first;
- (void)addStateSignalForProperty:(SEL)property;
- (void)addStreamSignalForProperty:(SEL)property;

@end
