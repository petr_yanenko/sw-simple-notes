//
//  APPSObserver.m
//  Wazere
//
//  Created by Petr Yanenko on 4/6/15.
//  Copyright (c) 2015 iOS Developer. All rights reserved.
//

#import "APPSObserver.h"
#import "RACDisposable.h"

@implementation APPSObserver

- (instancetype)initWithObserver:(id)observer handler:(ObserverHandler)handler {
  self = [super init];
  if (self) {
    _observer = observer;
    _handler = handler;
  }
  return self;
}

@end
