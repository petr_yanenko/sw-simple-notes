//
//  APPSNotesPolicy.h
//  SW-Simple-Notes
//
//  Created by Petr Yanenko on 9/24/16.
//  Copyright © 2016 Petr Yanenko. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface APPSNotePolicy : NSEntityMigrationPolicy

@end
