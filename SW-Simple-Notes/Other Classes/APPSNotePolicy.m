//
//  APPSNotesPolicy.m
//  SW-Simple-Notes
//
//  Created by Petr Yanenko on 9/24/16.
//  Copyright © 2016 Petr Yanenko. All rights reserved.
//

#import "APPSNotePolicy.h"
#import "APPSDBNote.h"
#import "APPSDBContent.h"
#import "APPSErrorHandler.h"
#import "NSObject+Introspection.h"

@implementation APPSNotePolicy

- (BOOL)createDestinationInstancesForSourceInstance:(NSManagedObject *)sInstance
                                      entityMapping:(NSEntityMapping *)mapping
                                            manager:(NSMigrationManager *)manager
                                              error:(NSError * _Nullable *)error {
    NSString *contentKey =  @"content";
    NSString *lastEditedKey = @"lastEdited";
    NSString *selectedKey = @"selected";
    NSSet<APPSDBContent *> *contentsCollection = [sInstance valueForKey:contentKey];
    NSMutableArray<NSString *> *contentArray = [[NSMutableArray alloc] initWithCapacity:contentsCollection.count];
    [contentsCollection enumerateObjectsUsingBlock:^(APPSDBContent * _Nonnull obj, BOOL * _Nonnull stop) {
        [contentArray addObject:obj.content];
    }];
    NSError *jsonError;
    NSData *contentData = [NSJSONSerialization dataWithJSONObject:contentArray options:0 error:&jsonError];
    BOOL result = NO;
    if (contentData == nil) {
        [ErrorHandler handleErrorInDomain:self.apps_className withCode:0 description:jsonError.localizedDescription];
        if (error != NULL) {
            *error = jsonError;
        }
    } else {
        NSManagedObject *newNote = [NSEntityDescription insertNewObjectForEntityForName:mapping.destinationEntityName inManagedObjectContext:manager.destinationContext];
        [newNote setValue:contentData forKey:contentKey];
        [newNote setValue:[sInstance valueForKey:lastEditedKey] forKey:lastEditedKey];
        [newNote setValue:[sInstance valueForKey:selectedKey] forKey:selectedKey];
        [manager associateSourceInstance:sInstance withDestinationInstance:newNote forEntityMapping:mapping];
        result = YES;
    }
    return result;
}

@end
