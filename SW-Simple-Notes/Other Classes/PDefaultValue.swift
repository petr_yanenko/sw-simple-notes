//
//  PDefaultValue.swift
//  SW-Simple-Notes
//
//  Created by Petr Yanenko on 8/19/16.
//  Copyright © 2016 Petr Yanenko. All rights reserved.
//

import Foundation

protocol PDefaultValue {
    static func defaultValue () -> Self;
}