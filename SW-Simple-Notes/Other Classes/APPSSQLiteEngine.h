//
//  APPSSQLIteManager.h
//  Simple Notes
//
//  Created by Petr Yanenko on 8/9/16.
//  Copyright © 2016 Petr Yanenko. All rights reserved.
//

#import <Foundation/Foundation.h>

#define SQLite [APPSSQLiteEngine sharedInstance]

@interface APPSSQLiteEngine : NSObject

@property (copy, nonatomic, readonly) NSMutableArray<NSMutableArray<NSString *> *> *data;
@property (copy, nonatomic, readonly) NSMutableArray<NSString *> *columnNames;

+ (instancetype)sharedInstance;

- (void)addPreparedStatement:(NSString *)statement forKey:(NSString *)key;
- (void)removePreparedStatementForKey:(NSString *)key;

- (void)executeStatements:(NSString *)statements;
- (void)executePreparedStatementWithKey:(NSString *)key values:(NSArray<NSString *> *)values;

@end
