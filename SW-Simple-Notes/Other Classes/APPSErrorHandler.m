//
//  APPSErrorHandler.m
//  Simple Notes
//
//  Created by Petr Yanenko on 8/9/16.
//  Copyright © 2016 Petr Yanenko. All rights reserved.
//

#import "APPSErrorHandler.h"
#import <Analytics.h>

@implementation APPSErrorHandler

+ (instancetype)sharedInstance {
    static APPSErrorHandler *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[APPSErrorHandler alloc] init];
    });
    return instance;
}

- (void)sendExceptionWithDescription:(NSString *)exceptionDescription {
    // May return nil if a tracker has not already been initialized with a
    // property ID.
    id tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder
                    createExceptionWithDescription:exceptionDescription  // Exception description. May be truncated to 100 chars.
                    withFatal:@NO] build]
     ];  // isFatal (required). NO indicates non-fatal exception.
}

- (void)handleErrorInDomain:(NSString *)domain withCode:(NSInteger)code description:(NSString *)description {
    
    
    NSString *exceptionDescription = [NSString stringWithFormat:@"%@/%ld/%@", domain, (long)code, description];
    
    [self sendExceptionWithDescription:exceptionDescription];
}

- (void)handleException:(NSException *)exception {
    NSString *reason = exception.reason;
    NSArray *callStack = exception.callStackSymbols;
    NSString *appName = [[NSString alloc] initWithFormat:@" %@ ", [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleName"]];
    for (NSString *frame in callStack) {
        if ([frame containsString:appName]) {
            NSRange addressRange = [frame rangeOfString:@"0x"];
            NSRange spaceRange = [frame rangeOfCharacterFromSet:[NSCharacterSet whitespaceCharacterSet]
                                                        options:NSLiteralSearch
                                                          range:NSMakeRange(addressRange.location + addressRange.length, frame.length - addressRange.location - addressRange.length)];
            reason = [reason stringByAppendingFormat:@"\n%@", [frame substringFromIndex:spaceRange.location + spaceRange.length]];
            break;
        }
    }
    [self sendExceptionWithDescription:reason];
}

@end
