//
//  AppDelegate.m
//  SW-Simple-Notes
//
//  Created by Petr Yanenko on 8/12/16.
//  Copyright © 2016 Petr Yanenko. All rights reserved.
//

#import "AppDelegate.h"
#import "SW_Simple_Notes-Swift.h"
#import "APPSGlobalDataContext.h"
#import <GGLAnalytics/GGLAnalytics.h>
#import <GAI.h>

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    // Configure tracker from GoogleService-Info.plist.
    NSError *configureError;
    [[GGLContext sharedInstance] configureWithError:&configureError];
    NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
    
    // Optional: configure GAI options.
    GAI *gai = [GAI sharedInstance];
    gai.trackUncaughtExceptions = YES;  // report uncaught exceptions
    gai.logger.logLevel = kGAILogLevelVerbose;  // remove before app release
    
    UIViewController *rootController;
    FoldersViewController *masterViewController = [[FoldersViewController alloc] init];
    NavigationController *masterNavigation = [[NavigationController alloc] initWithRootViewController:masterViewController];
    rootController = masterNavigation;
    if ([UIScreen mainScreen].traitCollection.userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        ContentViewController *detailsViewController = [masterViewController createContentViewContoller];
        NavigationController *detailsNavigation = [[NavigationController alloc] initWithRootViewController:detailsViewController];
        
        ContainerViewController *split = [[ContainerViewController alloc] init];
        split.viewControllers = @[masterNavigation, detailsNavigation];
        rootController = split;
    }
    
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.rootViewController = rootController;
    [self.window makeKeyAndVisible];
    
    [[APPSGlobalDataContext sharedInstance] attachObserver:self forProperty:@selector(activityIndicatorCounter) handler:^(APPSGlobalDataContext *subject) {
        NSInteger count = subject.activityIndicatorCounter;
        dispatch_async(dispatch_get_main_queue(), ^{
            [UIApplication sharedApplication].networkActivityIndicatorVisible = count > 0;
        });
    }];

    //Touch keyboard controller
    [KeyboardController keyboardInfo];

    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
