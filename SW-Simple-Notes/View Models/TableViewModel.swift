//
// Created by Petr Yanenko on 8/20/16.
// Copyright (c) 2016 Petr Yanenko. All rights reserved.
//

import Foundation

class TableViewModel<T : BaseCellViewModel> : IOSViewModel<[T]>, PTableViewModel {

    override init(model: APPSModel) {
        super.init(model: model);
    }

    func cellReuseIdentifier(_ indexPath: IndexPath?) -> String {
        return "";
    }

    func rowHeight() -> CGFloat {
        return self.rowHeight(0);
    }

    func rowHeight(_ tableHeight: CGFloat) -> CGFloat {
        return self.rowHeight(tableHeight, indexPath: nil);
    }

    func rowHeight(_ tableHeight: CGFloat, indexPath: IndexPath?) -> CGFloat {
        return 0;
    }

    func selectRowAction(_ indexPath: IndexPath) -> Void {
        self._selectRow(indexPath);
    }

    func cellViewModel(_ indexPath: IndexPath) -> T {
        return _data [indexPath.row];
    }

    func sectionsNumber() -> Int {
        return self.cellsNumber(0) > 0 ? 1 : 0;
    }

    func cellsNumber(_ section: Int) -> Int {
        return _data.count;
    }

// MARK: protected
    func _selectRow(_ indexPath: IndexPath) -> Void {

    }

    func _rowsCount(_ batch: Int) -> Int {
        return 0;
    }

    func _batchesCount() -> Int {
        return 1;
    }

    override func _createViewData() -> [T] {
        var data = [T]();
        for i in 0..<self._rowsCount(0) {
            let item = self._initializeItem();
            item.configure(IndexPath(row: i, section: 0));
            data.append(item);
        }
        return data;
    }

    func _initializeItem() -> T {
        let item = T(viewModel: self, model: _model);
        return item;
    }

}

extension Array : PDefaultValue {
    static func defaultValue() -> Array {
        return [];
    }
}
