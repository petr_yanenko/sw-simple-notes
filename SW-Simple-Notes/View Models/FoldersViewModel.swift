//
// Created by Petr Yanenko on 8/21/16.
// Copyright (c) 2016 Petr Yanenko. All rights reserved.
//

import Foundation

class FoldersViewModel : EditableTableViewModel<FoldersCellModel> {

    var foldersModel : APPSFoldersModel { get { return _baseListModel as! APPSFoldersModel; } }

    dynamic var showInsertDialog : Bool = false;

    init(foldersModel: APPSFoldersModel) {
        super.init(baseListModel: foldersModel);
        self.addStreamSignal(forProperty: #selector(getter: FoldersViewModel.showInsertDialog));
    }

    func insertFolderAction() -> Void {
        self.showInsertDialog = true;
    }

    func saveInsertedFolderAction(_ folder: String) -> Void {
        self.showInsertDialog = false;
        let trimmedFolder = folder.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        if trimmedFolder.characters.count > 0 {
            self._addApplicationTask {
                [weak self] in
                if let strongSelf = self {
                    strongSelf.foldersModel.insertFolder(withTitle: trimmedFolder);
                }
            }
        }
    }

    func cancelInsertFolderAction() -> Void {
        self.showInsertDialog = false;
    }

// MARK: protected
    override func _addObservers(_ model: APPSModel) -> Void {
        super._addObservers(foldersModel);
        self.foldersModel.attachObserver(self, forProperty: #selector(getter: APPSListModel.itemIsSelected), handler: {
            [weak self] subject in
            if let strongSelf = self {
                if strongSelf.foldersModel.itemIsSelected {
                    strongSelf._sendSelectRow(strongSelf.foldersModel.selectedItem.objectID);
                }
            }
        });
    }

}
