//
// Created by Petr Yanenko on 8/18/16.
// Copyright (c) 2016 Petr Yanenko. All rights reserved.
//

import Foundation

protocol PViewModel : APPSObserverProtocol {
    //protocols does not support dynamic properties (Command failed due to signal: Segmentation fault: 11)
//    var loading : Bool { get };
//
//    var newData : Bool { get };
//
//    var error : Bool { get };
    var errorTitle : String? { get };
    var errorMessage : String? { get };

    func reloadAction() -> Void;
    func loadDataAction() -> Void;
    func cancelAction() -> Void;
}