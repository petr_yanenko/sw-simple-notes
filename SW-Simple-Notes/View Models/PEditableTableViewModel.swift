//
// Created by Petr Yanenko on 8/20/16.
// Copyright (c) 2016 Petr Yanenko. All rights reserved.
//

import Foundation

protocol PEditableTableViewModel : PTableViewModel {
    //protocols does not support dynamic properties (Command failed due to signal: Segmentation fault: 11)
    var deletedPath : IndexPath { get };
//    var deleteRow : BOOL { get };

    var selectedPath : IndexPath { get };
//    var selectRow : Bool { get };

//    var highlightSelectedRow : Bool { get };

    var fromIndexPath : IndexPath { get };
    var toIndexPath : IndexPath { get };
//    var moveRow : Bool { get };

    var reloadIndexPath : IndexPath { get };
//    var reloadRow : Bool { get };

    func deleteRowAction(_ indexPath : IndexPath) -> Void;
    func subtitleCellViewModel(_ indexPath : IndexPath) -> SubtitleCellModel

    func editingDidStart() -> Void;
    func editingDidEnd() -> Void;
}
