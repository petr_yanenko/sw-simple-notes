//
// Created by Petr Yanenko on 8/18/16.
// Copyright (c) 2016 Petr Yanenko. All rights reserved.
//

import Foundation

typealias Action = () -> Void;

class BaseViewModel<T : PDefaultValue> : APPSBaseSubject, PViewModel {

    var _model : APPSModel;

    var _data : T;
    var data : T { get { return _data; } }

    dynamic var error : Bool = false;
    var _errorTitle : String?;
    var _errorMessage : String?;
    var errorTitle : String? { get { return _errorTitle; } }
    var errorMessage : String? { get { return _errorMessage; } }
    
    dynamic var loading : Bool = false;
    
    dynamic var newData : Bool = false;

    deinit {
        _model.detachObserver(self);
    }

    init(model: APPSModel) {
        _model = model;
        _data = T.defaultValue();
        super.init();
        self._addObservers(model);
        self.addStateSignal(forProperty: #selector(getter: APPSModel.loading));
        self.addStreamSignal(forProperty: #selector(getter: APPSModel.newData));
        self.addStreamSignal(forProperty: #selector(getter: BaseViewModel.error));
    }

    func reloadAction() -> Void {
        self._addApplicationTask {
            [weak self] in
            if let strongSelf = self {
                strongSelf._reload();
            }
        }
    }

    func loadDataAction() -> Void {
        self._addApplicationTask {
            [weak self] in
            if let strongSelf = self {
                strongSelf._loadData();
            }
        }
    }

    func cancelAction() -> Void {
        self._disableGUITasks();
        _model.detachObserver(self);
        
        self._addApplicationTask {
            [weak self] in
            if let strongSelf = self {
                strongSelf._cancel();
            }
        }
    }

// MARK: protected
    func _disableGUITasks() -> Void {

    }

    func _addGUITask(_ action : @escaping Action) -> Void {

    }

    func _checkGUITasksCount() -> Bool {
        return false;
    }

    func _addApplicationTask(_ action : @escaping Action) -> Void {

    }

    func _createViewData() -> T? {
        return nil;
    }

    func _reload() -> Void {
        _model.reset();
        _model.loadData();
    }
    
    func _loadData() -> Void {
        _model.loadData();
    }
    
    func _cancel() -> Void {
        _model.cancel();
    }
    
    func _addObservers(_ model : APPSModel) -> Void {
        _model.attachObserver(self, forProperty: #selector(getter: APPSModel.loading), handler: {
            [weak self] subject in
            if let strongSelf = self {
                let loading = strongSelf._model.loading;
                strongSelf._addGUITask({
                    strongSelf.loading = loading;
                });
            }
        });
        _model.attachObserver(self, forProperty: #selector(getter: BaseViewModel.error), handler: {
            [weak self] subject in
            if let strongSelf = self {
                let errorReason = strongSelf._model.errorReason;
                strongSelf._addGUITask({
                    strongSelf._sendError(errorReason!);
                });
            }
        });
        _model.attachObserver(self, forProperty: #selector(getter: APPSModel.newData), handler: {
            [weak self] subject in
            if let strongSelf = self {
                strongSelf._setData();
            }
        });
    }
    
    func _setData() -> Void {
        let data = self._createViewData();
        if let unwrapped = data {
            self._setData(unwrapped);
        }
    }
    
    func _setData(_ data: T) -> Void {
        self._addGUITask {
            [weak self] in
            if let strongSelf = self {
                strongSelf._GUITaskSetData(data);
            }
        }
    }
    
    func _GUITaskSetData(_ data: T) -> Void {
        _data = data;
        self.newData = true;
        self._dataDidSet();
    }
    
    func _dataDidSet() -> Void {
        
    }
    
    func _sendError(_ errorReason: String) -> Void {
        let error = self._createError(errorReason);
        _errorTitle = error.title;
        _errorMessage = error.message;
        self.error = true;
    }
    
    func _createError(_ errorReason: String) ->(title: String, message: String) {
        return ("", "");
    }
}
