//
//  IOSViewModel.swift
//  SW-Simple-Notes
//
//  Created by Petr Yanenko on 8/19/16.
//  Copyright © 2016 Petr Yanenko. All rights reserved.
//

import Foundation

class IOSViewModel<T : PDefaultValue> : BaseViewModel<T>, PiOSViewModel {

    var _guiTasksCount : Int = 0;
    
    var _disableGUITasksFlag : Bool = false;
    
    let _lockQueue = DispatchQueue(label: "Home.SW-Simple-Notes.LockQueue", attributes: []);
    
    var _operations = [Operation]();
    
    var navigationTitle: String { get { return ""; } };
    
    override init(model: APPSModel) {
        super.init(model: model);
    }

// MARK: protected
    override func _disableGUITasks() -> Void {
        _lockQueue.sync(execute: {
            self._disableGUITasksFlag = true;
        });
    }

    override func _addGUITask(_ action: @escaping Action) {
        _lockQueue.sync(execute: {
            if self._disableGUITasksFlag {
                return;
            }
            self._guiTasksCount += 1;
            for operation in self._operations {
                operation.cancel();
            }
            self._operations.removeAll(keepingCapacity: true);
        });
        DispatchQueue.main.async(execute: {
            self._lockQueue.sync(execute: {
                self._guiTasksCount -= 1;
                if self._disableGUITasksFlag {
                    return;
                }
            });
            action();
        });
    }

    override func _checkGUITasksCount() -> Bool {
        var result = false;
        self._lockQueue.sync {
            result = self._guiTasksCount == 0;
        }
        return result;
    }

    override func _addApplicationTask(_ action: @escaping Action) {
        _lockQueue.sync(execute: {
            if self._guiTasksCount == 0 || self._disableGUITasksFlag {
                let operation = APPSBlockOperation(timeDelta: 0, block: action);
                self._operations.append(operation!);
                operation!.start();
            } else {
                print("Application task was declined");
            }
        })
    }

}
