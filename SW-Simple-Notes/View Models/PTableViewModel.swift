//
// Created by Petr Yanenko on 8/20/16.
// Copyright (c) 2016 Petr Yanenko. All rights reserved.
//

import Foundation

protocol PTableViewModel : PViewModel {
    func sectionsNumber() -> Int;
    func cellsNumber(_ section: Int) -> Int;
    func cellReuseIdentifier(_ indexPath: IndexPath?) -> String;
    func rowHeight(_ tableHeight: CGFloat, indexPath: IndexPath?) -> CGFloat;
    func selectRowAction(_ indexPath: IndexPath) -> Void;
}
