//
// Created by Petr Yanenko on 8/21/16.
// Copyright (c) 2016 Petr Yanenko. All rights reserved.
//

import Foundation

class NotesViewModel : EditableTableViewModel<NotesCellModel> {

    var notesModel : APPSNotesModel { get { return _baseListModel as! APPSNotesModel; } }

    init(notesModel: APPSNotesModel) {
        super.init(baseListModel: notesModel);
    }

    func createNewNote() -> Void {
        self._addApplicationTask {
            [weak self] in
            if let strongSelf = self {
                strongSelf.notesModel.insertNote();
            }
        }
    }

    override func cancelAction() {
        self.notesModel.contentModel.detachObserver(self);
        super.cancelAction();
    }

// MARK: protected
    override func _reload() -> Void {
        self.notesModel.reset();
        self._loadData();
    }

    override func _loadData() -> Void {
        self.notesModel.loadData();
    }

    override func _addObservers(_ model: APPSModel) -> Void {
        super._addObservers(model);
        self.notesModel.attachObserver(self, forProperty: #selector(getter: APPSListModel.itemIsSelected), handler:{
            [weak self] subject in
            if let strongSelf = self {
                if let note = strongSelf.notesModel.selectedItem {
                    let noteID = note.objectID;
                    strongSelf._sendSelectRow(noteID!);
                }
            }
        });
        self.notesModel.contentModel.attachObserver(self, forProperty: #selector(getter: APPSModel.newData)) {
            [weak self] subject in
            if let strongSelf = self {
                let note = strongSelf.notesModel.selectedItem;
                if let selectedNote = note {
                    let selectedNoteID = selectedNote.objectID;
                    let content : [String]? = selectedNote.content;
                    let date = selectedNote.lastEdited;
                    strongSelf._addGUITask {
                        var index = 0;
                        for i in 0..<strongSelf._data.count {
                            let cellModel : NotesCellModel = strongSelf._data[i];
                            if selectedNoteID == cellModel.objectID {
                                cellModel.configureTitle(content, lastEdited: date!);
                                break;
                            }
                            index += 1;
                        }
                        if index > 0 {
                            if index < strongSelf._data.count {
                                let movedCell = strongSelf._data[index];
                                strongSelf._data.remove(at: index);
                                strongSelf._data.insert(movedCell, at: 0);
                                strongSelf.fromIndexPath = IndexPath(row: index, section: 0);
                                strongSelf.moveRow = true;
                            }
                        }
                        strongSelf.reloadIndexPath = IndexPath(row: 0, section: 0);
                    }
                }
            }
        }
    }

    override func _createViewData() -> [NotesCellModel] {
        var noteCells = super._createViewData();
        noteCells = noteCells.sorted(by: {
            previousCell, nextCell in
            let result = nextCell.lastEdited.compare(previousCell.lastEdited);
            return result == ComparisonResult.orderedAscending;
        });
        return noteCells;
    }
}
