//
// Created by Petr Yanenko on 9/11/16.
// Copyright (c) 2016 Petr Yanenko. All rights reserved.
//

import Foundation
import MobileCoreServices
import AVFoundation
import CoreMedia

class ContentViewModel : IOSViewModel<[NSMutableAttributedString]> {

    let kIndexKey = "index";
    let kTypeKey = "type";
    let kAttachmentKey = "attachment";
    let kAspectRatioKey = "ratio";
    let kAttachmentPathKey = "path";

    let kAttachmentMargin : CGFloat = 10;
    let kContentFont : CGFloat = 20.0;

    var _selectedNote : APPSNote?;

    var _attachmentCache = NSCache<AnyObject, AnyObject>();

    var containerControllerBounds : CGRect = CGRect.zero;
    var textViewBounds : CGRect = CGRect.zero;
    var selectedRange : NSRange = NSRange();
    var isPhone : Bool = false;
    var _changingLocation : Int = 0;
    var changingLocation : Int { get { return _changingLocation; } }

    dynamic var enabled : Bool = false;

    dynamic var showAttachmentDialog : Bool = false;

    var _pickerType : UIImagePickerControllerSourceType = UIImagePickerControllerSourceType.photoLibrary;
    var pickerType : UIImagePickerControllerSourceType { get { return _pickerType; } }

    var _showImagePicker : Bool = false;
    dynamic var showImagePicker : Bool {
        get { return _showImagePicker; }
        set { _showImagePicker = newValue; }
    }

    override var navigationTitle : String { get { return ""; } }

    var _attachmentPath : String = "";
    var attachmentPath : String { get { return _attachmentPath; } }
    dynamic var showOriginImage : Bool = false;
    dynamic var showOriginVideo : Bool = false;

    var _rotation : Bool = false;

    var contentModel : APPSContentModel { get { return _model as! APPSContentModel; } }

    init(contentModel: APPSContentModel) {
        super.init(model: contentModel);
        self.addStateSignal(forProperty: #selector(getter: ContentViewModel.enabled));
        self.addStreamSignal(forProperty: #selector(getter: ContentViewModel.showAttachmentDialog));
        self.addStateSignal(forProperty: #selector(getter: ContentViewModel.showImagePicker));
        self.addStreamSignal(forProperty: #selector(getter: ContentViewModel.showOriginImage));
        self.addStreamSignal(forProperty: #selector(getter: ContentViewModel.showOriginVideo));
        self._addApplicationTask {
            [weak self] in
            if let strongSelf = self {
                strongSelf._setData();
            }
        }
    }

    func content() -> NSMutableAttributedString {
        let content = NSMutableAttributedString();
        for (_, notePart) in _data.enumerated() {
            var item = notePart;
            var attributes = item.attributes(at: 0, effectiveRange: nil);
            let attachment: Data? = attributes[kAttachmentKey] as? Data;
            if let unwrappedAttachment = attachment {
                let textAttachment = NSTextAttachment(data: unwrappedAttachment, ofType: String(kUTTypeImage));
                if !self.isPhone {
                    let attachmentWidth = self._attachmentWidth(self.textViewBounds.width);
                    let aspectRatio: NSNumber? = attributes[kAspectRatioKey] as? NSNumber;
                    var attachmentHeight: CGFloat = 0;
                    if let unwrappedRatio = aspectRatio {
                        attachmentHeight = self._attachmentHeight(attachmentWidth, aspectRatio: CGFloat(unwrappedRatio.floatValue));
                    }
                    textAttachment.bounds = CGRect(x: 0, y: 0, width: attachmentWidth, height: attachmentHeight);
                }
                var attachmentString = NSMutableAttributedString(attributedString: NSAttributedString(attachment: textAttachment));
                let attachmentRange = NSRange(location: 0, length: attachmentString.length);
                attributes.removeValue(forKey: kAttachmentKey);
                attachmentString.addAttributes(attributes, range: attachmentRange);
                attachmentString = self._addAlignment(attachmentString, alignment: NSTextAlignment.center, range: attachmentRange);
                item.setAttributedString(attachmentString);
            } else {
                let style: NSParagraphStyle? = item.attribute(NSParagraphStyleAttributeName, at: 0, effectiveRange: nil) as? NSParagraphStyle;
                if style == nil {
                    item = self._addAlignment(item, alignment: NSTextAlignment.left, range: NSRange(location: 0, length: item.length));
                }
            }
            content.append(item);
        }
        let contentRange = NSRange(location: 0, length: content.length);
        content.addAttribute(NSFontAttributeName, value: UIFont.systemFont(ofSize: kContentFont), range: contentRange);
        return content;
    }

    func createNewNote() -> Void {
        self._addApplicationTask {
            [weak self] in
            if let strongSelf = self {
                if strongSelf.contentModel.notesModel.selectedFolder != nil {
                    strongSelf.contentModel.insertNewNote();
                }
            }
        }
    }

    func shouldChangeContent(_ content: NSAttributedString, range: NSRange, text: String) -> Bool {
        let content = self.content();
        var changingLocation = range.location;
        if text.characters.count > 0 {
            changingLocation += text.characters.count;
        }
        if _data.count > 0 {
            var attributesRange = NSRange();
            let index = self._notePartIndex(content, range: range, attributesRange: &attributesRange);
            self._changeNotePart(index, location: range.location - attributesRange.location, length: range.length, text: text, changingLocation: changingLocation);
        } else if text.characters.count > 0 {
            self._insertNotePart(0, location: 0, length: 0, content: text, type: kTextCode, changingLocation: changingLocation);
        }
        return false;
    }

    func addAttachmentAction() -> Void {
        if self.enabled {
            self.showAttachmentDialog = true;
        }
    }

    func photoLibraryAction() -> Void {
        self.attachmentMenuItemAction(UIImagePickerControllerSourceType.photoLibrary);
    }

    func cameraAction() -> Void {
        self.attachmentMenuItemAction(UIImagePickerControllerSourceType.camera);
    }

    func attachmentMenuItemAction(_ pickerType: UIImagePickerControllerSourceType) -> Void {
        self.showImagePicker = false;
        _pickerType = pickerType;
        self.showImagePicker = true;
        self.showAttachmentDialog = false;
    }

    func cancelPickerDialogAction() -> Void {
        self.showAttachmentDialog = false;
    }

    func dismissImagePickerAction() -> Void {
        self.showImagePicker = false;
    }

    func cancelShowingImagePickerAction() -> Void {
        self.showImagePicker = false;
    }

    func rotateAction() -> Void {
        self._addApplicationTask {
            [weak self] in
            if let strongSelf = self {
                strongSelf._rotation = true;
                strongSelf._setData();
                strongSelf._rotation = false;
            }
        }
    }

    func insertImageAttachmentAction(_ image: UIImage, content: NSAttributedString, selectedRange: NSRange) -> Void {
        if (!self._checkGUITasksCount()) {
            return;
        }
        let content = self.content();
        let bounds = self.containerControllerBounds;
        self._insertAttachment(content, selectedRange: selectedRange, type: kImageCode, dataCount: _data.count > 0) {
            let aspectRatio = self._aspectRatio(image);
            let resizedImage = self._resizeImage(image, bounds: bounds);
            let attachmentPath = self._saveAttachment(
                    resizedImage,
                    originExtension:
                    APPSConstants.sharedInstance().originImageExtension,
                    previewText: "image") {
                originPath in
                let data: Data? = UIImageJPEGRepresentation(image, 1.0);
                if let unwrappedData = data {
                    if !((try? unwrappedData.write(to: URL(fileURLWithPath: originPath), options: [.atomic])) != nil) {
                        APPSErrorHandler.sharedInstance().handleError(inDomain: self.apps_className, withCode: 4, description: "Image was note saved");
                    }
                } else {
                    APPSErrorHandler.sharedInstance().handleError(inDomain: self.apps_className, withCode: 5, description: "Image data was note created");
                }
            }
            return self._attachmentTuple(aspectRatio, path: attachmentPath);
        }
    }

    func insertMovieAttachmentAction(_ url: String, content: NSAttributedString, selectedRange: NSRange) -> Void {
        if (!self._checkGUITasksCount()) {
            return;
        }
        let content = self.content();
        let random = arc4random();
        let tempPath = APPSConstants.sharedInstance().tempDirectory.appendingFormat("%d.%@", random, APPSConstants.sharedInstance().originMovieExtension);
        let bounds = self.containerControllerBounds;
        let width = self._attachmentWidth(self._minDimensions(bounds));
        let dataCount = _data.count > 0;
        if FileManager.default.apps_moveItem(atPath: url, toPath: tempPath, errorDomain: self.apps_className, errorCode: 9) {
            self._insertAttachment(content, selectedRange: selectedRange, type: kVideoCode, dataCount: dataCount) {
                let image = self._createThumbnailImageFromURL(tempPath, thumbWidth: width, thumbHeight: width * 2);
                if let thumbImage = image {
                    let aspectRatio = self._aspectRatio(thumbImage);
                    let resizedImage = self._resizeImage(thumbImage, bounds: bounds);
                    let attachmentPath = self._saveAttachment(
                            resizedImage,
                            originExtension: APPSConstants.sharedInstance().originMovieExtension,
                            previewText: "movie"
                            ) {
                        originPath in
                        FileManager.default.apps_moveItem(
                                atPath: tempPath,
                                toPath: originPath,
                                errorDomain: self.apps_className,
                                errorCode: 10
                                );
                    }
                    return self._attachmentTuple(aspectRatio, path: attachmentPath);
                }
                return nil;
            }
        }
    }

    func attachmentTapAction(_ text: NSAttributedString, characterIndex: Int) -> Void {
        var effectiveRange: NSRange = NSRange();
        let attributes = text.attributes(at: characterIndex, effectiveRange: &effectiveRange);
        let type = attributes[kTypeKey] as? String;
        var path = attributes[kAttachmentPathKey] as? String;
        path = path?.components(separatedBy: ".").first;
        let format = ".%@";
        if let unwrappedType = type {
            if unwrappedType == kImageCode {
                path = path?.appendingFormat(format, APPSConstants.sharedInstance().originImageExtension);
                self._setAttachmentPath(path, event: &self.showOriginImage);
            } else if unwrappedType == kVideoCode {
                path = path?.appendingFormat(format, APPSConstants.sharedInstance().originMovieExtension);
                self._setAttachmentPath(path, event: &self.showOriginVideo);
            }
        }
    }

// MARK: protected
    override func _addObservers(_ model: APPSModel) {
        super._addObservers(model);
        let handler: ObserverHandler = {
            [weak self] subject in
            if let strongSelf = self {
                strongSelf._setData();
            }
        }

        let selector = #selector(getter: APPSListModel.itemIsSelected);
        self.contentModel.notesModel.attachObserver(self, forProperty: selector, handler: handler);
        self.contentModel.notesModel.foldersModel.attachObserver(self, forProperty: selector, handler: handler);
    }

    override func _setData() -> Void {
        let note = self.contentModel.selectedNote;
        let reset = _selectedNote == nil || note == nil || _selectedNote!.objectID != note?.objectID;
        _selectedNote = note;
        let data = self._createViewData();
        self._setData(data, reset: reset, changingLocation: self.contentModel.changingLocation);
    }

    func _setData(_ data: [NSMutableAttributedString]?, reset: Bool, changingLocation: Int) -> Void {
        let rotation = _rotation;
        let enabled = _selectedNote?.objectID != nil;
        self._addGUITask {
            [weak self] in
            if let strong = self {
                strong._changingLocation = changingLocation;
                if rotation || reset {
                    strong._changingLocation = 0;
                }
                if reset {
                    strong.selectedRange = NSRange(location: 0, length: 0);
                    strong.showAttachmentDialog = false;
                    strong.showImagePicker = false;
                }
                strong._GUITaskSetData(data ?? []);
                strong.enabled = enabled;
            }
        }
    }

    override func _createViewData() -> [NSMutableAttributedString]? {
        if let note = _selectedNote {
            var index = 0;
            var viewData: [NSMutableAttributedString] = [];
            for item in note.content {
                let content = item.substring(from: item.characters.index(item.startIndex, offsetBy: kTypeLength));
                let type = item.substring(to: item.characters.index(item.startIndex, offsetBy: kTypeCodeLength));
                let attributes = [kIndexKey: NSNumber(value: index as Int), kTypeKey: type] as [String : Any];
                var contentPart: NSMutableAttributedString;
                if type == kTextCode {
                    contentPart = NSMutableAttributedString(string: content, attributes: attributes);
                } else {
                    var aspectRatio: CGFloat = 1;
                    let path = self.contentModel.mediaPath(withContent: content, aspectRatio: &aspectRatio);
                    var file = _attachmentCache.object(forKey: path as AnyObject) as? Data;
                    var filePath = APPSConstants.sharedInstance().resourceDirectory.appendingFormat("/%@", path!);
                    if file == nil {
                        filePath = filePath.components(separatedBy: ".").first!;
                        filePath = filePath.appendingFormat(".%@", APPSConstants.sharedInstance().previewImageExtension);
                        file = try? Data(contentsOf: URL(fileURLWithPath: filePath));
                        if let attachment = file {
                            _attachmentCache.setObject(attachment as AnyObject, forKey: content as AnyObject);
                        }
                    }
                    let attachmentString = NSMutableAttributedString(string: " ");
                    let range = NSRange(location: 0, length: attachmentString.length);
                    attachmentString.addAttribute(kIndexKey, value: NSNumber(value: index as Int), range: range);
                    attachmentString.addAttribute(kTypeKey, value: type, range: range);
                    if let attachment = file {
                        attachmentString.addAttribute(kAttachmentKey, value: attachment, range: range);
                    }
                    attachmentString.addAttribute(kAspectRatioKey, value: NSNumber(value: Float(aspectRatio) as Float), range: range);
                    attachmentString.addAttribute(kAttachmentPathKey, value: filePath, range: range);
                    contentPart = attachmentString;
                }
                index += 1;
                viewData.append(contentPart);
            }
            return viewData;
        }
        return nil;
    }

}

// MARK: private
extension ContentViewModel {

    func _setAttachmentPath(_ path: String?, event: inout Bool) {
        if let attachmentPath = path {
            _attachmentPath = attachmentPath;
            event = true;
        } else {
            APPSErrorHandler.sharedInstance().handleError(inDomain: self.apps_className, withCode: 7, description: "Path is nil");
        }
    }

    func _addAlignment(_ oldString: NSMutableAttributedString, alignment: NSTextAlignment, range: NSRange) -> NSMutableAttributedString {
        let attachmentStyle = NSMutableParagraphStyle();
        attachmentStyle.alignment = alignment;
        oldString.addAttribute(NSParagraphStyleAttributeName, value: attachmentStyle, range: range);
        return oldString;
    }

    func _resizeImage(_ originImage: UIImage, bounds: CGRect) -> UIImage {
        let aspectRatio = self._aspectRatio(originImage);
        let width = self._attachmentWidth(self._minDimensions(bounds));
        let size = CGSize(width: width, height: self._attachmentHeight(width, aspectRatio: aspectRatio));
        UIGraphicsBeginImageContext(size);
        originImage.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height));
        let resizedImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return resizedImage!;
    }

    func _drawText(_ text: String, image: UIImage) -> UIImage {
        let fontSize : CGFloat = 36.0;
        let font = UIFont.boldSystemFont(ofSize: fontSize);
        UIGraphicsBeginImageContext(image.size);
        let rect = CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height);
        image.draw(in: rect);
        let textRect = CGRect(x: 0, y: (rect.size.height - fontSize) / 2, width: rect.size.width, height: rect.size.height / 2);
        let paragraphStyle = NSMutableParagraphStyle();
        paragraphStyle.lineBreakMode = NSLineBreakMode.byWordWrapping;
        paragraphStyle.alignment = NSTextAlignment.center;
        let attributes = [NSFontAttributeName: font, NSParagraphStyleAttributeName: paragraphStyle, NSForegroundColorAttributeName: UIColor.white];
        text.draw(in: textRect, withAttributes: attributes);
        let newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return newImage!;
    }

    func _saveAttachment(_ preview: UIImage, originExtension: String, previewText: String, saveHandler: (String)->Void) -> String? {
        let attachmentPath = self.contentModel.fullFilePath(_selectedNote);
        let format = "%@.%@";
        let previewPath = String(format: format, attachmentPath!, APPSConstants.sharedInstance().previewImageExtension);
        let originPath = String(format: format, attachmentPath!, originExtension);
        let image = self._drawText(NSLocalizedString("Tap to view " + previewText, comment: "Preview image title"), image: preview);
        let data : Data? = UIImageJPEGRepresentation(image, 1.0);
        var success = false;
        if let unwrappedData = data {
            if !((try? unwrappedData.write(to: URL(fileURLWithPath: previewPath), options: [.atomic])) != nil) {
                APPSErrorHandler.sharedInstance().handleError(inDomain: self.apps_className, withCode: 0, description: "Preview was not saved");
            } else {
                success = true;
            }
        } else {
            APPSErrorHandler.sharedInstance().handleError(inDomain: self.apps_className, withCode: 1, description: "Preview was not created");
        }
        if success {
            saveHandler(originPath);
            return attachmentPath;
        }
        return nil;
    }

    func _createThumbnailImageFromURL(_ videoPath: String, thumbWidth: CGFloat, thumbHeight: CGFloat) -> UIImage? {
        let fileURL = URL(fileURLWithPath: videoPath);
        let asset = AVURLAsset(url: fileURL);
        let generator = AVAssetImageGenerator(asset: asset);
        generator.appliesPreferredTrackTransform = true;

        let thumbTime = CMTime(value: 1, timescale: 60);
        let maxSize = CGSize(width: thumbWidth, height: thumbHeight);
        generator.maximumSize = maxSize;
        var thumbImage : UIImage?;
        do {
            let cgImage = try generator.copyCGImage(at: thumbTime, actualTime: nil);
            thumbImage = UIImage(cgImage: cgImage);
        } catch let error as NSError {
            APPSErrorHandler.sharedInstance().handleError(inDomain: self.apps_className, withCode: 2, description: error.localizedDescription);
        }
        return thumbImage;
    }

    func _insertAttachment(_ content: NSAttributedString, selectedRange: NSRange, type: String, dataCount: Bool, prepareContent: @escaping ()->(CGFloat, String)?) -> Void {
        var attributesRange : NSRange = NSRange();
        var index = 0;
        var location = 0;
        var length = 0;
        if dataCount {
            index = self._notePartIndex(content, range: selectedRange, attributesRange: &attributesRange);
            location = selectedRange.location - attributesRange.location;
            length = selectedRange.length;
        }
        let changedLength = APPSConstants.sharedInstance().attachmentLength;
        let changingLocation = selectedRange.location + changedLength;
        self._addContentTask(changingLocation) {
            [weak self] in
            if let strongSelf = self {
                let preparedContent : (CGFloat, String)? = prepareContent();
                if let result = preparedContent {
                    var fileName = result.1.components(separatedBy: "/").last;
                    fileName = fileName?.components(separatedBy: ".").first;
                    if let file = fileName {
                        let noteID = strongSelf._selectedNote!.percentEncodedObjectID;
                        if let unwrappedNoteID = noteID {
                            let notePart = String(
                                    format: "%f%@%@/%@",
                                    result.0,
                                    APPSConstants.sharedInstance().attachmentDivider,
                                    unwrappedNoteID,
                                    file
                                    );
                            strongSelf.contentModel.insertNotePart(with: index, location: location, length: length, content: notePart, type: type);
                        } else {
                            APPSErrorHandler.sharedInstance().handleError(inDomain: strongSelf.apps_className, withCode: 3, description: "Resource note folder was note created");
                        }
                    } else {
                        APPSErrorHandler.sharedInstance().handleError(inDomain: strongSelf.apps_className, withCode: 6, description: "File name is invalid");
                    }
                }
            }
        }
        self.showImagePicker = false;

    }

    func _notePartIndex(_ content: NSAttributedString, range: NSRange, attributesRange: inout NSRange) -> Int {
        var location = range.location;

        if range.location == content.length {
            location -= 1;
        }

        let attributes = content.attributes(at: location, effectiveRange: &attributesRange);
        let number = attributes[kIndexKey] as? NSNumber;
        var index = 0;
        if let unwrapped = number {
            index = unwrapped.intValue;
        }
        return index;
    }

    func _insertNotePart(_ index: Int, location: Int, length: Int, content: String, type: String, changingLocation: Int) -> Void {
        self._addContentTask(changingLocation) {
            [weak self] in
            if let strongSelf = self {
                strongSelf.contentModel.insertNotePart(with: index, location: location, length: length, content: content, type: type);
            }
        }
    }

    func _changeNotePart(_ index: Int, location: Int, length: Int, text: String, changingLocation: Int) -> Void {
        self._addContentTask(changingLocation) {
            [weak self] in
            if let strongSelf = self {
                strongSelf.contentModel.changeNotePart(with: index, location: location, length: length, text: text);
            }
        }
    }
    
    func _addContentTask(_ changingLocation: Int, action: @escaping ()->Void) {
        self._addApplicationTask {
            [weak self] in
            if let strongSelf = self {
                if (strongSelf._selectedNote?.objectID == strongSelf.contentModel.selectedNote.objectID) {
                    strongSelf.contentModel.changingLocation = changingLocation;
                    action();
                }
            }
        }
    }

    func _attachmentWidth(_ width: CGFloat) -> CGFloat {
        return width - kAttachmentMargin * 2;
    }

    func _minDimensions(_ bounds: CGRect) -> CGFloat {
        return min(bounds.size.width, bounds.size.height);
    }

    func _attachmentHeight(_ width: CGFloat, aspectRatio: CGFloat) -> CGFloat {
        return width * aspectRatio;
    }

    func _aspectRatio (_ image: UIImage) -> CGFloat {
        return image.size.height / image.size.width;
    }

    func _attachmentTuple(_ aspectRatio: CGFloat, path: String?) -> (CGFloat, String)? {
        return path != nil ? (aspectRatio, path!) : nil;
    }

}
