//
// Created by Petr Yanenko on 8/21/16.
// Copyright (c) 2016 Petr Yanenko. All rights reserved.
//

import Foundation

class FoldersCellModel : SubtitleCellModel {

    var _count : UInt = 0;
    override var subtitle : String { get { return String(_count) + " " + NSLocalizedString("notes", comment: "Notes count at folder"); } }

    override func configure(_ indexPath: IndexPath) -> Void {
        super.configure(indexPath);
        let folder = self._folder(indexPath);
        _title = folder.title;
        _count = folder.count;
    }

// MARK: protected
    override func _dataObject(_ indexPath: IndexPath) -> APPSObject {
        return self._folder(indexPath);
    }

}

// MARK: private
extension FoldersCellModel {

    func _folder(_ indexPath: IndexPath) -> APPSFolder {
        return self.model.item(at: indexPath.row) as! APPSFolder;
    }

}
