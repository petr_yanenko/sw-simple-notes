//
// Created by Petr Yanenko on 8/20/16.
// Copyright (c) 2016 Petr Yanenko. All rights reserved.
//

import Foundation
import CoreData

class SubtitleCellModel : BaseCellViewModel {

    var _objectID : String = "";
    var objectID : String { get { return _objectID; } }

    var _title : String = "";
    var title : String { get { return _title.trimmingCharacters(in: CharacterSet.newlines); } };

    var _subtitle : String = "";
    var subtitle : String { get { return _subtitle; } }

    var selected : Bool = false;

    var model : APPSListModel { get { return _model as! APPSListModel; } }

    override func configure(_ indexPath: IndexPath) -> Void {
        self._setData(fromObject: self._dataObject(indexPath));
    }

// MARK: protected
    func _dataObject(_ indexPath: IndexPath) -> APPSObject {
        return APPSObject();
    }

    func _setData(fromObject object: APPSObject) -> Void {
        _objectID = object.objectID;
        selected = object.selected;
    }

}
