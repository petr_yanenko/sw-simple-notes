//
// Created by Petr Yanenko on 8/22/16.
// Copyright (c) 2016 Petr Yanenko. All rights reserved.
//

import Foundation

class NotesCellModel : SubtitleCellModel {

    var _lastEdited : Date = Date(timeIntervalSince1970: 0);
    var lastEdited : Date { get { return _lastEdited; } }

    lazy var _dateFormatter : DateFormatter = self._createDateFormatter();

    override var subtitle : String {
        get { return NSLocalizedString("Last edited at", comment: "Notes cell subtitle") + " " +  _dateFormatter.string(from: _lastEdited); }
    }

    override func configure(_ indexPath: IndexPath) -> Void {
        super.configure(indexPath);
        let note = self._note(indexPath);
        self.configureTitle(note.content, lastEdited: note.lastEdited);
    }

    func configureTitle(_ content: [String]?, lastEdited: Date) {
        var type : String = "";
        var fragment : String = "";
        var firstFragment : String?;
        if let unwrappedContent = content {
            for fragment in unwrappedContent {
                let trimmedFragment = fragment.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
                if trimmedFragment.characters.count > kTypeLength {
                    firstFragment = trimmedFragment;
                    break;
                }
            }
        }
        if let unwrappedFragment = firstFragment {
            fragment = unwrappedFragment;
            type = fragment.substring(with: Range(fragment.startIndex..<fragment.characters.index(fragment.startIndex, offsetBy: kTypeCodeLength)));
        }
        if type == kTextCode {
            _title = fragment.substring(from: fragment.characters.index(fragment.startIndex, offsetBy: kTypeLength));
        } else if type == kImageCode {
            _title = NSLocalizedString("Photo", comment: "Notes cell photo title");
        } else if type == kVideoCode {
            _title = NSLocalizedString("Video", comment: "Notes cell video title");
        } else {
            _title = "";
        }

        _lastEdited = lastEdited;
    }

// MARK: protected
    override func _dataObject(_ indexPath: IndexPath) -> APPSObject {
        return self._note(indexPath);
    }

}

// MARK: private
extension NotesCellModel {

    func _note(_ indexPath: IndexPath) -> APPSNote {
        return self.model.item(at: indexPath.row) as! APPSNote;
    }

    func _createDateFormatter() -> DateFormatter {
        let formatter = DateFormatter();
        formatter.dateFormat = APPSConstants.sharedInstance().dateFormat;
        return formatter;
    }

}
