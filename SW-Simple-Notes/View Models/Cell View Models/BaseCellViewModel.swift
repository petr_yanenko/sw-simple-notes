//
// Created by Petr Yanenko on 8/20/16.
// Copyright (c) 2016 Petr Yanenko. All rights reserved.
//

import Foundation

class BaseCellViewModel : NSObject {

    weak var _viewModel : PViewModel?;
    let _model : APPSModel;

    required init(viewModel: PViewModel, model: APPSModel) {
        _viewModel = viewModel;
        _model = model;
        super.init();
    }

    func configure(_ indexPath: IndexPath) -> Void {

    }
}
