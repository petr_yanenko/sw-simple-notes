//
// Created by Petr Yanenko on 8/20/16.
// Copyright (c) 2016 Petr Yanenko. All rights reserved.
//

import Foundation

class EditableTableViewModel<T : SubtitleCellModel> : TableViewModel<T>, PEditableTableViewModel {

    var _baseListModel : APPSListModel { get { return _model as! APPSListModel; } }

    var deletedPath : IndexPath {
        didSet {
            deleteRow = true;
        }
    };
    dynamic var deleteRow : Bool = false;

    var selectedPath : IndexPath;
    dynamic var selectRow : Bool = false;
    dynamic var highlightSelectedRow : Bool = false;

    var fromIndexPath : IndexPath;
    var toIndexPath : IndexPath { get { return IndexPath(row: 0, section: 0); } }
    dynamic var moveRow : Bool = false;

    var reloadIndexPath : IndexPath {
        didSet {
            reloadRow = true;
        }
    };
    dynamic var reloadRow : Bool = false;

    override var navigationTitle : String { get { return NSLocalizedString("Notes", comment: "Application navigation title"); } }

    init(baseListModel: APPSListModel) {
        let emptyPath = IndexPath();
        deletedPath = emptyPath;
        selectedPath = emptyPath;
        fromIndexPath = emptyPath;
        reloadIndexPath = emptyPath;
        super.init(model: baseListModel);
        self.addStreamSignal(forProperty: #selector(getter: EditableTableViewModel.deleteRow));
        self.addStreamSignal(forProperty: #selector(getter: EditableTableViewModel.selectRow));
        self.addStreamSignal(forProperty: #selector(getter: EditableTableViewModel.highlightSelectedRow));
        self.addStreamSignal(forProperty: #selector(getter: EditableTableViewModel.moveRow));
        self.addStreamSignal(forProperty: #selector(getter: EditableTableViewModel.reloadRow));
    }

    override func cellReuseIdentifier(_ indexPath: IndexPath?) -> String {
        return "Default cell";
    }

    override func rowHeight(_ tableHeight: CGFloat, indexPath: IndexPath?) -> CGFloat {
        return 44.0;
    }

    func deleteRowAction(_ indexPath: IndexPath) -> Void {
        let cellModel = _data [indexPath.row];
        _data.remove(at: indexPath.row);
        self.deletedPath = indexPath;
        let objectID = cellModel.objectID;
        
        self._addApplicationTask {
            [weak self] in
            if let strongSelf = self {
                strongSelf._baseListModel.deleteItem(objectID);
            }
        }
    }

    func editingDidStart() -> Void {

    }

    func editingDidEnd() -> Void {
        var i = 0;
        for cellModel in _data {
            if cellModel.selected {
                self.selectedPath = IndexPath(row: i, section: 0);
                self.highlightSelectedRow = true;
                break;
            }
            i += 1;
        }
    }

    func subtitleCellViewModel(_ indexPath: IndexPath) -> SubtitleCellModel {
        return self.cellViewModel(indexPath);
    }

// MARK: protected
    func _deleteItem(_ objectID: String) -> Void {
        _baseListModel.deleteItem(objectID);
    }

    override func _rowsCount(_ batch: Int) -> Int {
        return _baseListModel.count;
    }

    func _selectItem(_ objectID: String) -> Void {
        _baseListModel.selectItem(objectID);
    }

    override func _selectRow(_ indexPath: IndexPath) -> Void {
        let cellModel = _data [indexPath.row];
        let objectID = cellModel.objectID;
        
        self._addApplicationTask {
            [weak self] in
            if let strongSelf = self {
                strongSelf._baseListModel.selectItem(objectID);
            }
        }
    }

    func _sendSelectRow(_ objectID: String) -> Void {
        self._addGUITask {
            [weak self] in
            if let strong = self {
                var index = 0;
                for cellModel in strong._data {
                    cellModel.selected = false;
                    if cellModel.objectID == objectID {
                        cellModel.selected = true;
                        strong.selectedPath = IndexPath(row: index, section: 0);
                        strong.selectRow = true;
                    }
                    index += 1;
                }
            }
        }
    }

}
