
#import "Constants.h"

@implementation APPSConstants

+ (instancetype)sharedInstance {
    static APPSConstants *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[APPSConstants alloc] init];
    });
    return instance;
}

- (NSString *)documentsDirectory {
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
}

- (NSString *)tempDirectory {
    return NSTemporaryDirectory();
}

- (NSString *)resourceDirectory {
    return [self.documentsDirectory stringByAppendingPathComponent:@"Resources"];
}

- ( NSString *)originImageExtension {
    return @"img";
}

- (NSString *)originMovieExtension {
    return @"mov";
}

- (NSString *)previewImageExtension {
    return @"prv";
}

- (NSString *)attachmentDivider {
    return @"/";
}

- (UIFont *)navigationBarFont {
    return [UIFont fontWithName:@"HelveticaNeue-Light" size:17.f];
}

- (UIColor *)mainBackgroundColor {
    return [UIColor darkGrayColor];
}

- (UIColor *)mainTintColor {
    return [UIColor whiteColor];
}

- (UIColor *)mainTextColor {
    return [UIColor whiteColor];
}

- (NSString *)dateFormat {
    return @"MM/dd/yyyy HH:mm";
}

- (NSString *)okTitle {
    return NSLocalizedString(@"Ok", nil);
}

- (NSString *)createNoteTitle {
    return NSLocalizedString(@"New", nil);
}

- (NSInteger)attachmentLength {
    return @" ".length;
}

@end