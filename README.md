# README #

I developed simplified clone of Apple Notes app. Application layer and User Interface layer were mostly written on Swift 2.3. Business layer was written on Objective-C. Application skeleton is MVVM pattern using Reactive Cocoa framework. To build application you need Xcode 8.